%---------------------------------------------------------------------------------
%	CHAPTER Seven: Conclusion (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Conclusion]{Conclusion}
\chaptermark{Conclusion}
\label{ch:conclusion} % label for referring to chapter in other parts of the thesis

\newpage


% ----------------------------------------------------------------
% ----------------------------------------------------------------


\section{Summary of findings}

In Chapter \ref{ch:introduction} the aim of this thesis was defined to:
\textbf{demonstrate the importance of snow in a temperate climate - case study Scotland.}
The objectives for achieving this aim were to:

\begin{itemize}
\item Show that a volunteer-collected, snowline-observation dataset can be used to quantify snow duration and melt
\item Map and quantify the relationship between snow and the NAO index
\item Quantify Scottish snow extreme value statistics, i.e. snow cover and snowmelt.
\end{itemize}

I have demonstrated the importance of snow in Scotland, through a literature review in Chapter \ref{ch:introduction}.
This search found Scottish snow impacts flooding, ecology, traffic, recreation and landforms.
To support this, analysis in Chapter \ref{ch:results} quantified snowmelt as a proportion of precipitation; this is up to an annual median of 42\% for the snowiest areas, although for lowland areas this figure is less than 5\%.

Scottish SSGB data between 1945 and 2007 are now available in electronic, transcribed form.
They were collected from 140 sites covering mainland Scotland and a number of islands.
The longest station record is 52 years in length, which was collected at Couligarten and observed Ben Lomond.
This transcribed dataset is stored in the Met Office MIDAS database and is available through the British Atmospheric Data Centre\footnote{\url{http://badc.nerc.ac.uk}}.
The SSGB advantages and disadvantages are largely governed by study scale.
The main advantages are a long, 60 year, daily record of snow cover observations across Scotland, as recorded by knowledgeable and experienced local weather observers.
The disadvantages are that the SSGB covers discrete locations, observed by recorders working in isolation, with missing and lost records caused by observer absence or reduced visibility.
These enable the SSGB to be used at a mountain or catchment scale, or to provide spot checks for satellite and Met Office grid data when making national assessments.
Hence, using a mixture of datasets to reduce uncertainty is potentially the best way forward.
The SSGB offers 60 years of detailed daily records of snow cover from station level up to the highest mountains in Scotland; no other data product contains this resolution of information.

Spatial variability of snow cover is a big challenge, it is difficult to observe and quantify.
This is typified by the contrasting results of UKCP09 snow and MODIS data correlations with NAO index (Chapter \ref{ch:NAO}).
I have overcome this to correlate snow cover with NAO by using disparate snow cover datasets, encompassing anecdotal type data (Bonacina snowiness index), interpolated ground observed data (UKCP09), the SSGB and satellite observations (MODIS).
With the exception of the MODIS analysis, these have all shown the same results: that Scottish snow cover is generally negatively correlated with the NAO index, with stronger correlations at lower elevations and in southern and eastern Scotland.
This is in contrast to correlations with precipitation alone.
Precipitation, mostly as rain, was found to correlate to a proxy of the NAO index strongest in western Scotland and insignificantly in eastern Scotland \citep{Macdonald2006}.
This contrast between spatial correlations of precipitation phases and NAO index is most likely due to the different temperatures experienced during positive and negative phases of the NAO.
Results from individual SSGB stations and UKCP09 grids correlate well, demonstrating the value of UKCP09 data for national scale assessment of spatial trends.
At sample locations, snow lying between November and April increases by 6 to 16 days for each unit reduction in NAO index.
These estimates could be used in conjunction with seasonal NAO forecasts in preparation for upcoming winters, by groups like highways and local authority planners and snow sports industries.

Chapters \ref{ch:model} and \ref{ch:results} presented a simple snow accumulation and melt model.
This model was calibrated using Met Office point observations of snow depth and SSGB hillslope-scale snow cover data.
The calibration of this model took a simplified approach and did not address equifinality or define uncertainty.
The reasons for this were computing time and poor precipitation input data.
Precipitation input data used in the snow model were shown (Section \ref{section:MarLodge} to underestimate input to a mountainous catchment on the east side of the Cairngorms by a median value of 17\%, when compared to river flow.
The latter was a bigger restriction; even if it were possible to perfectly fit the model results to observations, underestimated precipitation at higher elevations would lead to erroneous results.
Despite an underestimate of high elevation precipitation, meaning there is less potential snowpack to melt, the model simulated some high snowmelt rates.
These high melt rates were generally confined to elevations above 400 m ASL, but even at 50\% AEP some snowmelt exceeded 42 mm/day, which is the currently recommended maximum used in flood structure design \citep{FEH1999}.
Considering snowmelt as an annual proportion of precipitation, allowed me to avoid the uncertainty of higher elevation precipitation.
Results from this analysis showed a general Scotland-wide decline between 1960 and 2010 in snowmelt as a proportion of precipitation.
However, upland areas around Ben Nevis showed the opposite; indicating that the highest mountains in the west of Scotland remain cold enough to accumulate snow even in a warming climate.
The spatial organisation of snowmelt as a proportion of precipitation are, unsurprisingly, similar to a reduction in duration of annual snow cover; there is a widespread reduction, particularly in the east.
Duration of snow cover results are less strongly correlated to NAO and show a weaker temporal trend (via correlation with year) than snowmelt and the extent of significant correlations less widespread.

This thesis is early work in the quantification of snowmelt, snow cover in Scotland.
The following section contains potential subsequent work that falls into two categories: addressing limitations of this thesis and research derived from this thesis.


% ----------------------------------------------------------------
% ----------------------------------------------------------------


\section{Scope for further work}


As new snow datasets become available, particularly from satellite and reanalysis products, it will be worthwhile revisiting and updating this research to help constrain uncertainty.
This will be particularly pertinent if predictions of a more volatile NAO index come to pass, as we will be better able to understand the link between snow cover and climate variability.
Uncertainty also exists in the distribution of snow cover.
The SSGB recorded at a hillslope scale, and so has inherent information as to the duration of snow cover on varying slope aspects.
South facing slopes face the sun more than north facing slopes and are, hence, warmer; comparisons like this could be used to indicate what may happen in a warming climate to snow cover in Scotland.

Better use should be made of currently collected data.
As this thesis has shown, new understanding can be found from re-analysing existing datasets like the SSGB.
Hopefully this thesis can be a catalyst for transcription of the remaining SSGB records, held in the Met Office archives (Exeter) and the Manley Archives (Durham).
Currently satellite observations of snow cover perform poorly in temperate, cloudy climates.
Improving the classification of snow cover from satellite data when temperatures are close to 0 \textdegree C and there is cloud would be of obvious use to other temperate regions of the world.
Results from this exercise could also be used to improve snow classification in the autumn and spring months of colder regions, where temperatures are closer to 0 \textdegree C and observation uncertainty consequently exists.
The machine learning approach shown in Chapter \ref{ch:data_validation}, which used ground based observations from the SSGB to train a decision tree model to classify MODIS observations into snow cover should be explored further.
This could include using other MODIS snow cover data products, for example the dataset which estimates the percentage of snow cover in each cell.
To make results from satellite snow cover most usable in a water resources context a spatial reclassification model could be developed which considered river catchments split by elevation band and aspect.
Splitting river catchments in this way would group topography into snow cover zones which had similar characteristics.
However, care would be needed over scale and topographic generalisation as MODIS snow cover data are available on a 500 m grid and it would be challenging to translate this grid size to represent complex mountain topography.

Another question that remains to be answered includes whether a warmer climate will lead to a higher number of snow accumulation and melt cycles each winter.
This could have one of two impacts: that with less time between melt phases less snow accumulates and so maximum melt rates are reduced, or that a warmer climate also brings more precipitation and the rate of snow accumulation increases, bringing more frequent snowmelt episodes which are of an equivalent size to those currently observed.
The most prominent potential impact of a change in snowmelt would be on flood risk.
As was demonstrated in Chapter \ref{ch:results}, potential snowmelt is underestimated in UK flood risk assessments.
Related work on snowmelt flood risk could investigate the impact of snowmelt on existing impoundment structures, e.g. dams, considering the potential risk of snowmelt to each structure, based on the results presented in Chapter \ref{ch:results}.

Throughout this thesis I have detailed areas which I believe warrant further investigation, these are discussed in the following paragraphs.
A priority for British snow modelling is an improvement in available input data.
In Section \ref{section:MarLodge} I detail the underestimation of precipitation by the CEH GEAR dataset in a high elevation, heavily snow influenced catchment.
It would be relatively straightforward to estimate the water balance for a comprehensive number of river gauging stations across Great Britain, to see if the CEH GEAR underestimate is systematic.
These results should be checked using observed precipitation lapse rates from a range of elevations.

An improved estimate of precipitation would enable the modelling exercise, detailed in Chapter \ref{ch:model}, to be repeated with a more realistic value for water input.
A flaw with the modelling presented in Chapter \ref{ch:model} is the limited number of parameter sets used during the grid calibration.
This should be addressed and uncertainty estimates made of the model outputs.

With a climate baseline of snowmelt and cover estimates, the presented snow model could be run using future projections of precipitation and temperature to help understand the impact a changing climate could have on Scottish water resources.
Another approach to this problem could be the use of SSGB data for calibration of more established and complex land surface models.

Much of the work of this thesis and potential subsequent research, described above, have wide applicability.
A key challenge is how can these be incorporated into policy?
What options are available for transferring hydro-meteorological, academic research into public and commercial use?
