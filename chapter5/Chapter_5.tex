%---------------------------------------------------------------------------------
%	CHAPTER Five: Model (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Snow modelling]{Snow accumulation and melt modelling}
\chaptermark{Modelling}
\label{ch:model} % label for referring to chapter in other parts of the thesis

\newpage


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Introduction} \label{section:mod_intro}


The purpose of this chapter is to estimate snowmelt and snow cover, through time, with spatially distributed results.
Hydrologically, snowmelt is often incorporated holistically into runoff models with assessment of melt trends and patterns limited by the conceptual model used \citep[e.g.][]{Bell2016}; i.e. if a study looks at the impact of snowmelt on river flows but only calibrates the model on river flows it may arrive at the right answer for the wrong reason and draw the wrong conclusions regarding changes to snowmelt patterns.
As discussed in Chapter \ref{ch:introduction}, spatially distributed knowledge of snow cover and melt are important for our understanding of the natural environment, including: extreme hydrological events \citep{Black1993} and ecology \citep{Helliwell1998}.

Snow cover and melt are popular research topics in cold climates like parts of the United States \citep[e.g. Colorado and Wyoming:][]{Fassnacht2014}, Canada \citep[e.g.][]{Pomeroy2003} and the Arctic basin \citep[e.g.][]{Lammers2001} where the hydrological challenge, particularly relating to climate change impacts, is more straightforward \citep{Jefferson2011} because snow is the dominant phase of winter precipitation.
Few studies consider snow cover and melt in temperate climates and, hence, it is poorly documented.

Methods to estimate snow cover and melt include point observations, inference from other observations (e.g. river flow/runoff), remote sensing and modelling.
Point observations, \citep[e.g.][]{Archer1981,Hough1997a} are limited as they are often unrepresentative, particularly of higher elevations.
Point observations can be interpolated to infill gaps, but they are often on a very coarse grid size \citep{Brasnett1999}.
\citet{Archer1981} compares point and runoff snowmelt estimation in NE England, finding that the former underestimates snowmelt runoff.
In \citet{Archer1981} point snowmelt values were derived from data at nine low elevation (<242 m) Met Office sites, using the duration of thaw from a known SWE starting point.
The snowmelt runoff estimation in \citet{Archer1981} is based on a network of 25 snow monitoring stations installed in 1979 by the Northumbrian Water Authority.
At these, observers recorded daily values of snow depth, density and SWE.
Values of SWE and precipitation were compared to direct runoff (i.e. total runoff - base flow) to derive the water released from the snow pack.
Snowmelt rates between 12 and 144 mm/day were observed, although it is noted that the separation between rain and snowmelt was difficult.
These two approaches are useful, but rely heavily on significant observer time and extrapolation between observation points.

SWE of dry snow can be remotely observed using microwave sensors, but climate and land surface complexities cause large uncertainties \citep{Dong2005}.
As discussed in Section \ref{section:MODIS_desc}, using microwave sensors to observe snow in Scotland is challenging as they find it difficult to quantify wet snow.
\citet{Foster2011} produce a blended snow cover product from multiple sources of remotely sensed data, which reduces the weakness associated with individual methods, but it is only available on a 25 km\textsuperscript{2} grid.

Another option to quantify snow cover and melt is through modelling; \citet{Essery2013} provide a comprehensive overview.
There are two main types of snow modelling: energy balance \citep[e.g.][]{Ferguson1987} and degree-day (also known as temperature index) \citep[Chap. 10]{DeWalle2008}.
Energy balance models are data intensive and hence difficult to use \citep{Bormann2014} and when \citet{Biggs2012} reviewed the available literature they found temperature index methods more appropriate than energy balance ones over large areas due to the rarity of required data inputs for the latter method.
\citet{Avanzi2016} compare a single layer degree-day model (HyS) with a multi-layer energy balance model (Crocus) and find the degree-day model performs comparably well at a daily time step when estimating snow depth, SWE and snow density.
Snowmelt models can be calibrated using remotely sensed data \citep[e.g.][]{Clark2006, Biggs2012}, but for work in Scotland this has problems previously discussed (Sections \ref{section:MODIS_desc} and \ref{section:SSGBvsMODIS}).
Some early examples of degree-day modelling in the UK include \citet{Archer1983} (Troutbeck, Harwood Beck and Langdon Beck in NE England) and \citet{Ferguson1984} (River Feshie in the Cairngorms, Scotland).
These used a lumped model for the whole catchment and included a routing component and compared model output to river flows successfully, but this method neither addresses spatial variations, nor answer questions on snow melt and cover.
More recently, \citet{Bell1999} developed the PACK model, which is essentially a distributed degree-day model with a snow store partitioned between wet and dry.
The PACK model was developed for flood forecasting and is now used in G2G (Grid to Grid) by organisations such as the Scottish Environment Protection Agency.
In other temperate climates, i.e. Australia, \citet{Bormann2014} have successfully constrained snowmelt results using simulated snow densities in a degree-day model.
\citet{Martinec1986} and \citet{Hock2003} summarise literature on parameters in snowmelt modelling.
\citet{Martinec1986} find degree-day factors (DDF), which govern the rate of melt, between 3.5 and 6 mm/\textdegree C/day, with lower values for less dense (newer) snow.
\citet{Martinec1986} also review temperature thresholds between rain and snow and note that these exceed 0\textdegree C and are highest in the spring months, e.g. 3\textdegree C, but are around 0.75\textdegree C at other times of the year.
\citet{Hock2003} only reviews DDF and find appropriate values between 2.5 to 5.5 mm/\textdegree C/day for non-glaciated sites.

To estimate snowmelt and cover, it is proposed here to use a daily, single-layer degree-day model of snow accumulation and melt.
Taking advantage of available SSGB data, this will be calibrated across a range of elevations and will use density estimates to improve performance.
The rest of this chapter details the methods used and discusses their appropriateness.


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Methods overview}  \label{section:model_methods}


A single layer degree-day (temperature index) snow model (shown schematically in Figure \ref{fig:model}), with code shown in Appendix \ref{ch:Appendix_mod}, was coded in the R language \citep{cran}.
The model is based on the methods described in \citet[Chapter 10.3.2]{DeWalle2008}.
This model takes observations of precipitation and temperature as input and calculates snow water equivalent (SWE) and snowmelt as main outputs.
There is a subroutine which estimates snow density and hence depth, which is used to diagnose 50\% snow cover in a given area and calibrate the model.
The model has been run on a daily time step, but could work at other temporal resolutions \citep[e.g.][]{Tobin2013}.

Model parameters are detailed in Table \ref{tab:mod_params}.
The two primary parameters which control the model are DDF (degree-day factor) and Temp.b.
The four other parameters in Table \ref{tab:mod_params} fine-tune model output or control the estimation of snow depth.


\begin{table}[htbp]
\caption{Model parameters.} \label{tab:mod_params}
\centering
\begin{tabular}{llp{9cm}}
\hline
Name & Units & Description \\
\hline
DDF & mm/day/ \textdegree C & Degree-day factor, which controls the quantity of melt when there is a snow pack. \\
Temp.b & \textdegree C & Temperature threshold between precipitation falling as snow or rain. \\
den & kg/m  \textsuperscript{3} & Density of snow on the first day it fell. \\
den.i & kg/m  \textsuperscript{3}/day & Daily increase of snow density. \\
DDF.d & km/m  \textsuperscript{3}/day & Daily decrease of degree-day factor. \\
d.50 & mm & Depth at which snow covers 50\% of given area. \\
\hline
\end{tabular}
\end{table}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/model.pdf}
\caption{Schematic of the single layer degree-day model used. Precip is daily precipitation; Temp is daily temperature; DDF is degree-day factor; Temp.b is temperature threshold for snow or rain; SWE is snow water equivalent; d.50 is depth at which snow cover for a given area is greater than 50\%. See Table \ref{tab:mod_params} for more detail.} \label{fig:model}
\end{figure}


The model was first run using observations from five Met Office stations: Braemar, Dalwhinnie, Eskdalemuir, Inverailort and Knockanrock (Table \ref{tab:MOstdata}).
This initial run was used to establish parameter space, using a wide range of values for all parameters excluding d.50.
The d.50 parameter is used to estimate snow cover greater than 50\% in a given area from snow depth.
The model was then run for a 5 km grid, at cells which overlapped SSGB observations across Scotland.
This gridded model run used 1960 to 1990 data for calibration and 1991 to 2005 (when the SSGB ended) data for verification.
Finally, the model was run on a 5 km grid covering all of Scotland, using those parameters which performed best when compared to the SSGB dataset.
This is, essentially, a simplified application of the GLUE (Generalised Likelihood Uncertainty Estimation) method \citep{Beven1992, Beven2006}, in which parameter sets which perform equally well are all used.
However, in this case, only those which perform best are carried through to the next stage of the process.
While this does not explicitly address the problem of equifinality or estimate uncertainty, it does allow derivation of an answer within the constraints of computational limitation by severely limiting the number of parameter sets used in the model runs which take the most time (i.e. grid simulations).
The following sections detail the individual Met Office station calibration, grid calibration and then model performance.


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Individual station calibration} \label{section:model_station}


A series of Met Office stations across Scotland, which covered a range of elevations and reported daily snow depth, precipitation and temperature was identified.
These five stations are shown in Figure \ref{fig:MOst_map} and detailed in Table \ref{tab:MOstdata}.
The number of winters (Oct to May) in which at least one of them reported all (244 or 245) days of observations was 19.
This count was reduced to 13 winters (Figure \ref{fig:MO_data_avail}) when subset by those stations that reported at least one day with a snow depth greater than zero; i.e. eliminating stations which reported no snow all winter.
Model input was daily temperature and precipitation, with output calibrated against observed snow depth.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/MO_data_avail.pdf}
\caption{Available winters with complete daily snow depth, precipitation and temperature data for chosen Met Office stations.} \label{fig:MO_data_avail}
\end{figure}

\begin{table}[htbp]
\caption{Model parameter space for Met Office station run.
See Table \ref{tab:mod_params} for parameter explanations, including units.} \label{tab:mod_params_values}
\centering
\begin{tabular}{lllr}
\hline
Name & Low & High & Increment \\
\hline
DDF & 2 & 7 & 0.5 \\
Temp.b & 0 & 1 & 0.25 \\
den & 120 & 180 & 10 \\
den.i & 0 & 6 & 1 \\
DDF.d & 0 & 0.06 & 0.01 \\
\hline
\end{tabular}
\end{table}


All parameters, except d.50, were varied as shown in Table \ref{tab:mod_params_values}, creating 18865 different combinations; d.50 was not included, as its value has no impact on snow depth.
These parameter limits (Table \ref{tab:mod_params_values}) were set by a coarse increment first run which, aided by literature discussed parameter values \citep[e.g.][]{Martinec1986,Hock2003,Essery2013}, were varied over orders of magnitude.
The purpose of running the snow model at these Met Office stations was to reduce the set of likely parameter values for the more computationally intensive gridded model run and to calibrate estimates of snow depth.

Model performance was evaluated by calculating the root mean squared error (RMSE) \citep{hydroGOF} for each parameter set.
Eight parameter sets were within 2\% of the lowest value of RMSE; these are shown in Table \ref{tab:mod_params_best}.
All parameters, except Temp.b, have variation.
A density of 120 kg/m\textsuperscript{3} performed best overall, which is comparable with physically sampled values of 100 to 150 kg/m\textsuperscript{3} reported in \citet{Archer1981} during the winters of 1977 and 1978 in Forest, Teesdale, England.
DDF.d (the density reduction factor) values of, or close to, zero performed well; this indicates that the d.50 parameter has little effect on the results.


\begin{table}[htbp]
\caption{Model parameters which were within 2\% of the lowest RMSE.
See Table \ref{tab:mod_params} for parameter explanations, including units.} \label{tab:mod_params_best}
\centering
\begin{tabular}{lllllr}
  \hline
DDF & Temp.b & den & den.i & DDF.d & RMSE \\ 
  \hline
2.00 & 0.50 & 120 &   1 & 0.01 & 23.00 \\ 
  2.00 & 0.50 & 120 &   2 & 0.01 & 22.73 \\ 
  2.00 & 0.50 & 130 &   1 & 0.01 & 22.98 \\ 
  2.00 & 0.50 & 130 &   2 & 0.01 & 23.14 \\ 
  3.00 & 0.50 & 120 &   3 & 0.00 & 23.04 \\ 
  3.50 & 0.50 & 120 &   2 & 0.00 & 23.03 \\ 
  4.00 & 0.50 & 120 &   2 & 0.00 & 23.03 \\ 
  4.50 & 0.50 & 120 &   2 & 0.00 & 23.03 \\ 
  \hline
\end{tabular}
\end{table}


The best performing parameter sets were then identified for each station and each year.
The modelled depth from these parameters was then plotted with observed depth (Figure \ref{fig:Depth_best_by_st}), which can be compared to best overall parameter depth (Figure \ref{fig:Depth_best_overall}).
As can be seen, some observed snow accumulations are missed in all simulations shown, notably for Dalwhinnie in 1979.
However, there is not a marked difference between the best parameter sets overall, and those which perform best at a given station for a given winter.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/Depth_best_by_st.pdf}
\caption{Plots of observed snow depth and modelled snow depth generated by the best performing parameter sets for each station and year.
Note that all stations Y-axes show snow depth (mm), circles are observations and lines are model output.
Note, the number of model results shown on each plot is 231, 2, 2, 11, 3, 2, 1, 2, 2, 1, 539, 7546 and 2 (starting top left, reading by row).} \label{fig:Depth_best_by_st}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/Depth_best_overall.pdf}
\caption{Plots of observed snow depth and modelled snow depth generated by the best performing parameters overall. Y-axes show snow depth (mm), circles are observations and lines are model output.} \label{fig:Depth_best_overall}
\end{figure}


The parameters shown in Table \ref{tab:mod_params_best}, which resulted in the snow depths shown in Figure \ref{fig:Depth_best_overall} are the ones used in the gridded model run.


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Grid calibration} \label{section:model_grid}


To calibrate the model on a grid for a range of elevations, the model was resolved on a per cell basis between October and May each winter for a range of cells which overlap areas visible from selected SSGB stations.
SSGB stations were selected (Table \ref{tab:SSGB_cali_st}) based on a long record, a range of visible elevations and to cover a broad geographical area of Scotland.
Viewsheds for each station were overlaid on the UKCP09 5km grid and those cells that intersected the visible envelopes were used for the gridded model calibration (Figure \ref{fig:SSGB_cali_map}).
There were 76 cells in total.


\begin{table}[htbp]
\caption{SSGB stations used for model calibration.} \label{tab:SSGB_cali_st}
\centering
\begin{tabular}{lp{1.8cm}p{1.8cm}p{1.8cm}p{5cm}}
  \hline
Name & Easting (m) & Northing (m) & Station elevation (m) & Visible hills, as noted by SSGB observer \\ 
  \hline
Ardtalnaig & 270200 & 739400 & 129 & Ben Lawers: 1214 m, 3 km N\&NW. Ground to E of station: 685 m. Ben More: 1174 m, 32 km WSW of station. \\ 
  Cassley PS & 239600 & 923200 & 100 & Ben More Assynt: 998 m. Maovally: 510 m. Ben Hee: 873 m. \\ 
  Eskdalemuir & 323500 & 602600 & 231 & Ettrick Pen: 692 m, 320\textdegree, 6 km. Lochfell: 300°, 688 m, 7 km. \\ 
  Fersit & 235100 & 778200 & 240 & None noted. \\ 
  Whitehillocks & 344860 & 779790 & 261 & Up to 914 m \\ 
   \hline
\end{tabular}
\end{table}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/SSGB_cali_map.pdf}
\caption{Location of model cells overlapping SSGB station viewsheds used for calibration.} \label{fig:SSGB_cali_map}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/SSGB_cali.pdf}
\caption{Number of available records (a) and mean cell elevations visible shown with a box and whisker plot (median, quartiles, range and outliers) (b) for the SSGB stations used to calibrate the grid model.} \label{fig:SSGB_cali}
\end{figure}


The model was calibrated on 1960 to 1990 data, with 1991 to 2005 data used for verification.
SSGB data which are available for these periods are shown in Figure \ref{fig:SSGB_cali}a with the range of elevations covered by each station illustrated in Figure \ref{fig:SSGB_cali}b.
Parameters assessed were the six selected in Table \ref{tab:mod_params_best}, coupled with a range of values for d.50.
d.50 varied between 0 and 300 mm with an increment of 30 mm, making a total of 66 parameter combinations.
When depth exceeded the value of d.50, the grid cell was presumed to have snow cover of greater than 50\%, the same percentage snow cover SSGB observers used to determine snowline.
This binary model output was used for calibration and verification against the SSGB dataset.
To match SSGB snowline to model output, the SSGB was converted to a binary presence of snow lying for each cell visible from each SSGB station.
Model performance was then evaluated using a penalised approach based on daily correctness, i.e. for each day that the model correctly predicted snow presence or absence it scored one and for each day it was incorrect it scored negative one.
Performance of the d.50 parameter was checked by taking the median performance value from each parameter set and plotting against the d.50 value used (Figure \ref{fig:d.50_overall}).
These show that the best performance is between 150 and 250 mm, broadly similar to \citet{Niu2007} values of between approximately 10 and 150 mm.
Beyond d.50 values of 250 mm there is equifinality.

The model performance was also plotted against model output (Figures \ref{fig:SSGB_cover_melt}a and b).
Both figures show model runs which performed very poorly, these are the result of d.50 being equal to zero.
What these figures also show is that with d.50 of zero, there is little impact on the total melt, but that snow cover duration almost doubles that observed by the SSGB.
An extreme test for the model would be to see how well these complete melts of snow correspond to long lying snow patch observations \citep[e.g.][]{Watson2011}.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/d50_overall.pdf}
\caption{Median model performance over 76 cells between 1960 and 1990 for the d.50 parameter.} \label{fig:d.50_overall}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/SSGB_cover_melt.pdf}
\caption{a) Total sum of modelled days with snow cover with a red line indicating the count observed by the SSGB and b) sum of snowmelt plotted against model performance. Both are for d.50 = 0.} \label{fig:SSGB_cover_melt}
\end{figure}


Calibrating the model against SSGB data allows comparison between results from a wide range of elevations.
Mean elevations in the 76 calibration cells range from 140 to 830 m.
Comparing the parameter performance between these elevation bands shows whether it is appropriate to use different parameter sets at different elevations.
Cell elevations were grouped to 150 m elevation bands, matching the SSGB observations, and then a similar plot to Figure \ref{fig:d.50_overall} was made, but points were colour coded by elevation (Figure \ref{fig:d.50_elev}).
What is apparent is the different response above 300 m, compared to Figure \ref{fig:d.50_overall} where model performance decreases as d.50 increases.
The difference between Figures \ref{fig:d.50_overall} and \ref{fig:d.50_elev} is because higher elevation cells are fewer and so their responses to changes in d.50 are overwhelmed by the greater number of lower elevation cells.
The reason there is a difference between higher and lower elevations is because presenting the d.50 parameter split by elevation is allowing it to behave as a model residual, i.e. showing unexplained differences in performance at different elevations.
I believe this is primarily due to CEH GEAR data underestimating higher elevation precipitation (see Section \ref{section:MarLodge}), causing d.50 to be much smaller than at lower elevations to compensate for a reduced snow accumulation.
The other point to note in Figure \ref{fig:d.50_elev} is the saturation of model performance at lower elevations for a higher value of d.50.
Lower elevation cells are more likely than high elevation cells to be too warm for snowfall and accumulation, and only in the coldest winters will there be many days with snow cover.
If the daily temperature does not fall below 1 to 1.5\textdegree C then the model will not accumulate snow, nor is it likely that any would lie in reality.
In these conditions it is very easy for the model, almost whatever parameters are chosen, to match all observations.
As the median value of model performance has been used in Figure \ref{fig:d.50_elev} then this is reflecting the larger proportion of years when no snow may have fallen at lower elevations.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/d50_elev.pdf}
\caption{Median model performance over 76 cells between 1960 and 1990 for the d.50 parameter, split by elevation.} \label{fig:d.50_elev}
\end{figure}


The best performing parameter set scored 0.731 and there were 12 other parameter sets which scored within 0.5\% of this.
Table \ref{tab:SSGB_cali_best} shows these parameters and their scores.
As can be seen, Temp.b and den.i have no variation, although the former was set at the station calibration phase.
den and DDF.d have little variation and DDF and d.50 have the largest variation.
Given the uncertainty of precipitation at higher elevations, the final model was run with the same parameter set at all elevations as varying parameters did not have enough justification and computing time was prioritised.
To do this, the best performing parameters were chosen; these are highlighted with horizontal lines in Table \ref{tab:SSGB_cali_best}.


\begin{table}[htbp]
\caption{Best performing model parameter sets compared to SSGB data, with those used enclosed with lines. Note a d.50 value of 120 mm was used for the final model run.} \label{tab:SSGB_cali_best}
\centering
\begin{tabular}{llllllr}
  \hline
DDF & Temp.b & den & den.i & DDF.d & d.50 & Performance \\ 
  \hline
2.0 & 0.5 & 120 & 2.0 & 0.010 & 270 & 0.728 \\ 
  \hline
  2.0 & 0.5 & 130 & 2.0 & 0.010 & 240 & 0.731 \\ 
  \hline
  2.0 & 0.5 & 130 & 2.0 & 0.010 & 300 & 0.728 \\ 
  3.5 & 0.5 & 120 & 2.0 & 0.000 & 240 & 0.728 \\ 
  3.5 & 0.5 & 120 & 2.0 & 0.000 & 270 & 0.728 \\ 
  3.5 & 0.5 & 120 & 2.0 & 0.000 & 300 & 0.728 \\ 
  4.0 & 0.5 & 120 & 2.0 & 0.000 & 240 & 0.728 \\ 
  4.0 & 0.5 & 120 & 2.0 & 0.000 & 270 & 0.728 \\ 
  4.0 & 0.5 & 120 & 2.0 & 0.000 & 300 & 0.728 \\ 
  4.5 & 0.5 & 120 & 2.0 & 0.000 & 240 & 0.728 \\ 
  4.5 & 0.5 & 120 & 2.0 & 0.000 & 270 & 0.728 \\ 
  4.5 & 0.5 & 120 & 2.0 & 0.000 & 300 & 0.728 \\ 
   \hline
\end{tabular}
\end{table}


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Model performance and complete run} \label{section:model_perf}


Using the best performing parameters highlighted in Table \ref{tab:SSGB_cali_best}, model calibration and validation performance were assessed.
The calibration period was 1960 to 1990 and the verification period 1991 to 2005.
The verification period finishes three years earlier than the input data (2010) as this is when the SSGB records end.
The performance of the calibration and verification periods is shown in Figure \ref{fig:cali_veri}.
Some values are less than zero, as a penalty was applied if the model did not agree with SSGB observations.
Model performance is better at lower elevations, where there is less snow and it is warmer, hence easier for the model to predict.
The model generally performs slightly better in the verification period, compared to the calibration period.
Again, this is thought to be because the winters beginning 1991 to 2005 were warmer than 1960 to 1990, meaning fewer days of snow for the model to predict.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/cali_veri.pdf}
\caption{Model performance for the calibration, 1960-1990 (a) and verification, 1991-2005 (b) periods, split by elevation and shown with a box and whisker plot (median, quartiles, range and outliers).} \label{fig:cali_veri}
\end{figure}


To test the theory that the model performs better when there is less snow, model performance was plotted against the number of days with snow cover (Figure \ref{fig:cali_veri_snowdays}) as recorded by the SSGB.
Performance decreases at all elevations as the number of days of snow cover increases.
As there are fewer high elevation cells, results for these are less conclusive.
The limitation of Figure \ref{fig:cali_veri_snowdays} is that there is no differentiation between snowy winters when the temperature fluctuated around 0\textdegree C and snowy winters which were much colder.
These differences are shown in Figure \ref{fig:cali_veri_temp}, which plots model performance, per cell, per winter, against the average minimum Scottish winter temperature (downloaded from the Met Office\footnote{\url{http://www.metoffice.gov.uk/climate/uk/summaries/datasets}}).
Note the y-axis varies between plots, to better show patterns.
Unexpectedly, there is a general peak in model performance around 0\textdegree C, although the lowest two elevation bands have a slight decrease in performance very close to 0\textdegree C.
This wider increase in model performance around 0\textdegree C could be due to these temperatures being the most common and so the most training data were available for the model.
At elevations including and below 600 m, model performance improves for the coldest and warmest winters, supporting my hypothesis that when precipitation is much more likely to fall as either snow or rain the model does not struggle to differentiate between the two.
This trend is not apparent in the 900 m elevation band, perhaps due to a paucity of model results and observations.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/cali_veri_snowdays.pdf}
\caption{Model performance for the 1960 to 2005 winters, split by elevation and plotted against the number of days with snow cover recorded by the SSGB. The plots are fitted with Loess and 95\% confidence limits. Each point represents one cell in one year.} \label{fig:cali_veri_snowdays}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter5/figures/cali_veri_temp.pdf}
\caption{Model performance for the 1960 to 2005 winters, split by elevation and plotted against the UKCP09 average, Scottish winter (DJF) temperature. The plots are fitted with Loess and 95\% confidence limits. Each point represents one cell in one year, note that the y axis scales differ between plots.} \label{fig:cali_veri_temp}
\end{figure}


The calibrated model was run for the whole of Scotland (approx 3000 5 km cells) for winters beginning in 1960 to 2010.
The daily output of SWE, snowmelt and snow cover (based on depth exceeding the d.50 parameter) grids are stored in NetCDF format.
These results were used to derive snowmelt and snow cover statistics which are shown in Chapter \ref{ch:results}.


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Discussion} \label{section:model_disc}


A degree-day snowmelt model, validated across a wide elevation range, has yielded daily snowmelt and snow cover data for a 50 year period.
This is of particular use in temperate climates where ephemeral snow makes annual snow pack estimation invalid, and where direct wide-scale observation of snow cover is difficult.
Absolute snowmelt results, particularly at higher elevations, are uncertain as precipitation input data appear to notably underestimate high elevation precipitation.
This means that high elevation snowmelt values are likely to be underestimated.
Despite this, I think there is great worth in looking at relative values of melt with the assumption that different locations with the same elevation are likely to be comparable and that differences between high and low elevations are likely to be underestimated.
As the model was calibrated on snow cover, results for snow cover are less likely to be affected by an underestimate of high elevation precipitation.
It would be of great use to the discipline of flood risk management to derive absolute values of snowmelt, to better inform the estimation of rare floods and protect communities.
However, to do this, better estimations of high elevation precipitation are needed, as this is where the greatest uncertainty lies.
With these improved input data and more computing resources it would be of interest to run the gridded model stochastically, using a wider range of suitable parameters giving a range of estimates for snowmelt and helping to understand its uncertainty.
The following chapter, \ref{ch:results}, considers the results from this modelling exercise.
