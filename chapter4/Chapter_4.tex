%---------------------------------------------------------------------------------
%	CHAPTER Four: NAO (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[North Atlantic Oscillation]{Relationship between snow cover and the North Atlantic Oscillation index}
\chaptermark{NAO}
\label{ch:NAO} % label for referring to chapter in other parts of the thesis


\vfill
\small{Author Contributions: The majority of the work presented in this chapter has been published with Richard Essery as a co-author \citep{Spencer2016}.
Essery contributed minor edits to the publication.}
\newpage

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Introduction} \label{section:intro}


The North Atlantic Oscillation (NAO) index is the normalised pressure difference between the Icelandic low and the Azores high \citep{Walker1932}.
During winter months positive NAO phases are typified by strong westerly winds carrying moist warm air from the Atlantic, with negative NAO phases bringing colder air masses from the east \citep{Hurrell1995, Simpson2014}.
Logically then, the NAO index could indicate the duration of snow cover in Scotland as colder weather means a greater chance of snow and its persistence, but this signal may be confused by positive NAO phases bringing increased precipitation.

NAO index relates to hydrological processes: \citet{Hannaford2005} show river flow and NAO index have strong positive correlations (e.g. River Nith: 0.63) in the north and west of the UK, but eastern catchments had a weaker correlation (e.g. River Tweed: 0.38).
\citet{Harrison2001} suggested that an association between snow cover and NAO phase is likely.
\citet{Trivedi2007} found snow cover in the Ben Lawers region north of Loch Tay below 300 m to be significantly negatively correlated with NAO index (-0.55 to -0.44, p<0.05), with lower elevations having a stronger relationship.
\citet{Trivedi2007} also found no correlation between NAO index and falling snow.
This could be because it is often cold enough for snow to fall during a Scottish winter, irrespective of NAO phase, but during positive NAO phases the warmer air causes snow to melt and only with the colder temperatures associated with negative NAO indices does snow lie for longer.
Another explanation could be that during a negative phase NAO there is less total precipitation, but a higher proportion of snowfall, meaning the quantity of snowfall stays approximately the same.
There has been more research on snow cover links to the NAO index in continental Europe, where snow cover has a greater impact \citep[e.g.][]{Beniston1997, Bednorz2004, Scherrer2004, Lopez-Moreno2011, Kim2013}).

There has recently been an increase in winter variability of the NAO phase \citep{Osborn2006, Hanna2014}, including a record low winter NAO index in 2009 to 2010 \citep{Osborn2010}.
The 2009/10 low occurred the same year as an exceptionally cold and snowy winter in the UK \citep{MetOffice2010, Prior2011}.
\citet{Goodkin2008} link variability in the NAO index to northern hemisphere mean temperature and state that any future predictions should take this into account.
The mean winter (DJFM) NAO indices are shown in Figure \ref{fig:NAO_timehist}.

The UK Met Office is beginning to forecast seasonal NAO indices more successfully \citep{Scaife2014}, which could be used to plan for heavy snow in advance of a winter season.
For a forecast made on the 1st of November, \citet{Scaife2014} give a correlation value of 0.62 (significant at 99\%) between forecast and observed DJF NAO indices for the years 1993 to 2012.

This chapter aims to quantify the relationship between Scottish snow cover and the NAO index.
I establish this by looking at nationwide snow cover datasets, before further investigating relationships at a hillslope scale, using case studies with more detailed data available.
This chapter is laid out as follows: data and methods, results, and discussion.
The methods and results sections are split by dataset.


% Phase often sets for winter so using relationship to help predict snow cover duration useful (only if I can find a reference for it).
% IPCC AR4 (hopefully 5 too) discuss NAO, this should be mentioned.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Data and methods} \label{section:d&m}


Snow in Scotland is often ephemeral and so metrics like average snowline and maximum snow cover extent are meaningless because each winter can see many snow accumulation and melt cycles.
I solved this by using a count of the days of snow cover during a given time period, defining a winter period for snow cover as November to April to help differentiate the snowiest winters, while being short enough to not discount many SSGB records, as some are missing \citep{Spencer2014}.
A short winter period (e.g. DJF) would mean, particularly at higher elevations, a sum of days with snow lying would result in saturated counts of snow cover duration.
For example, there cannot be more than 31 days with snow lying in January, but 31 days of cover is often the case at higher elevations in Scotland; so, choosing a shorter observation period means there is little or no distinction between an average winter and a very snowy one.
Using a six month period will help identify the snowiest winters, where greater snow depths take longer to melt.
Data used for this chapter are shown in Table \ref{tab:data}.
This section details the methods used to compare each data source to the NAO.


\begin{table}[htbp]
\caption{Data sources of snow cover and NAO index.} \label{tab:data}
\centering
\begin{tabular}{p{2.5cm}lp{2.5cm}p{3cm}r}
\hline
Name & Abbreviation & Reference & Type & Time span \\
\hline
Bonacina snowiness index & Bonacina & \citet{OHara_web} & Classification of snowiness of UK winter & 1875 onwards \\
\hline
UK Climate Projections 2009 snow lying grid & UKCP09 & \citet{Perry2005} & Interpolated grid of UK Met Office station data (days per month) & 1971 - 2006 \\
\hline
MODIS satellite snow cover, daily L3 500m grid v005 & MODIS & \citet{Hall2006} & Daily classified raster image & 2000 onwards \\
\hline
North Atlantic Oscillation index & NAO index & \citet{Osborn_web} & Single annual value (DJFM mean) & 1821 onwards \\
\hline
Snow Survey of Great Britain & SSGB & \citep{Spencer2014} & Daily observations of snowline elevation & 1945 - 2007 \\
\hline
\end{tabular}
\end{table}


%---------------------------------------------------------------------------------
\subsection{Bonacina}


Mean DJFM NAO index values are grouped by Bonacina categories.
The differences between groups of the NAO index are compared visually using boxplots (Figure \ref{fig:Bonacina_box}) and statistically using ANOVA and Tukey honest significant differences (HSD) \citep{Yandell1997} tests, the latter to account for family-wise analysis (Table \ref{tab:Bonacina}).


%---------------------------------------------------------------------------------
\subsection{UK Climate Projections 2009 (UKCP09)}


The November to April sums of days of snow cover from UKCP09 are compared to the mean DJFM NAO index using a Pearson correlation.
The resulting Pearson correlation is plotted (Figure \ref{fig:MOgrid_cor}) to show spatial patterns.


%---------------------------------------------------------------------------------
\subsection{SSGB}


SSGB stations which recorded all months between November and April are used in this chapter.
The number of valid stations per year is shown in Figure \ref{fig:SSGB_time}.

Snow accumulation curves, as shown in Section \ref{section:SSGB_trends}, are used and split by NAO index (Figure \ref{fig:SSGB_curves}).
The primary purpose of these curves is to assess the break point between higher and lower elevation snow cover.

Three groups of individual stations are also considered, again meeting the criterion of six months of record for a winter; group one: stations with the longest record, group two: stations in the east of Scotland, group three: a single station on Orkney.
Details of these stations are shown in Table \ref{tab:SSGB_long} and their location in Figure \ref{fig:SSGB_map_subset}.
The second and third groups have much shorter records than the longest-running stations; they have been included to help test whether eastern sites are more likely to have snow cover  influenced by the NAO index and whether the UKCP09 snow data are a good approximation of the relationship between snow cover and NAO.
The groups of stations in Table \ref{tab:SSGB_long} are compared to the NAO index using a high and low elevation split (at 750 m) and Loess (locally weighted scatterplot smoothing) \citep{Cleveland1979, Cleveland1988} with 95\% confidence limits (Figure \ref{fig:SSGB_ind_long} and \ref{fig:SSGB_ind_oth}).

Stations from Table \ref{tab:SSGB_long}, judged by eye to have a Loess close to a straight line, are plotted in Figure \ref{fig:SSGB_lm} with linear models, showing the Pearson correlation value and line parameters (slope and intercept).
This allows us to relate a given NAO index to an expected number of days snow cover duration for a high or low elevation.


%---------------------------------------------------------------------------------
\subsection{Moderate-resolution Imaging Spectroradiometer (MODIS)}


MODIS data from April to November each year were summed and correlated against the mean DJFM NAO index, presented in Figure \ref{fig:MODIS_cor}a.
Figure \ref{fig:MODIS_cor}b shows the same analysis, repeated for cloud cover observed by MODIS.

%---------------------------------------------------------------------------------
\subsection{Spatial scales comparison}

To relate SSGB station and national results, Pearson correlations from SSGB, MODIS and UKCP09 are compared.
Values from MODIS and UKCP09 rasters were extracted at SSGB station locations and are shown together in Table \ref{tab:NAO_correlations}.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Results}


%---------------------------------------------------------------------------------
\subsection{Bonacina}

Figure \ref{fig:Bonacina_box} shows boxplots of the difference between DJFM NAO indices as grouped by the Bonacina classification.
A general trend can be seen where less snowy winters have a more positive NAO index.
This is demonstrated statistically using ANOVA (F value = 25.07) and a Tukey HSD analysis (Table \ref{tab:Bonacina}) where each adjacent pair is shown with a best estimate of difference and significance value.
All pairs are different at greater than 5\% significance, except Very Snowy - Snowy.
This could be a result of the Very Snowy small sample size, for which the Tukey HSD test performs less well.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/BonacinaBox.pdf}
\caption{Boxplots (median, upper and lower quartiles and range) showing winter NAO indices grouped by Bonacina snowiness categories.} \label{fig:Bonacina_box}
\end{figure}

\begin{table}[htbp]
\caption{Tukey HSD difference in medians of NAO indicies between pairs of Bonacina classes.} \label{tab:Bonacina}
\centering
\begin{tabular}{llr}
\hline
Pair & Difference & P value \\
\hline
Very Snowy-Snowy & -0.823 & 0.093 \\
Snowy-Average & -0.670 & 0.008 \\
Average-Little & -0.697 & 0.002 \\
\hline
\end{tabular}
\end{table}


%---------------------------------------------------------------------------------
\subsection{UKCP09 snow}

Figure \ref{fig:MOgrid_cor} shows UKCP09 snow cover correlated with NAO across Scotland; some of these areas are strongly negatively correlated.
The strongest correlations are in the south west and along the east coast.
Areas of poor correlation are predominantly in central and northern mainland Scotland and Orkney.
There are two small areas of stronger correlation near Inverness and east of Skye.
Some of the poor correlation areas coincide with areas with few Met Office stations, notably northern Scotland.
However, data paucity is not an issue for Orkney and central Scotland.
Perhaps snow cover in these regions is much more affected by local weather systems than other areas.
Local weather systems could be effected by the presence of open water (e.g. Loch Lomond).
The higher mountains in central Scotland are likely to remain cold enough for snow to accumulate and linger, whatever the phase of the NAO index; thus rendering them less susceptible to changes in NAO index.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/MOgrid_map.pdf}
\caption{Map of Pearson correlation values between UKCP09 snow and the NAO index, for the period 1971 to 2006.
Contains Met Office data \copyright Crown copyright and database right 2016.} \label{fig:MOgrid_cor}
\end{figure}


%---------------------------------------------------------------------------------
\subsection{SSGB}


\begin{table}[htbp]
\caption{Longest, eastern and Orkney SSGB stations details.} \label{tab:SSGB_long}
\centering
\begin{tabular}{llllr}
\hline
Station & Easting & Northing & Description & Complete winters \\
\hline
Eskdalemuir & 323500 & 602600 & Longest & 46 \\
Couligarton & 245400 & 700700 & Longest & 44 \\
Forrest Lodge & 255500 & 586600 & Longest & 44 \\
Ardtalnaig & 270200 & 739400 & Longest & 39 \\
Fersit & 235100 & 778200 & Longest & 39 \\
\hline
Drummuir & 337200 & 844100 & Eastern & 24 \\
Derry Lodge & 303600 & 793200 & Eastern & 21 \\
Crathes & 375800 & 796900 & Eastern & 20 \\
Whitehillocks & 344860 & 779790 & Eastern & 27 \\
\hline
Stenness & 329800 & 1011200 & Orkney & 21 \\
\hline
\end{tabular}
\end{table}


Figure \ref{fig:SSGB_curves}, showing SSGB snow accumulation curves, displays a marked difference in duration of snow cover at all elevations between winters with the highest and lowest NAO indices, with positive NAO phases having less snow cover than negative NAO phases.
Below 750 m the changes in days of snow cover as elevation increases are broadly linear, while above 750 m the relationship is unclear, with lines crossing.
This 750 m change-point is used to distinguish between high and low snow cover for the SSGB station analysis.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/SSGB_curves_NAO.pdf}
\caption{Snow cover duration curves derived from SSGB data between 1946 and 2006 (Nov to Apr), grouped by (rounded) mean DJFM NAO index.} \label{fig:SSGB_curves}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/SSGB_loc.pdf}
\caption{Selected SSGB station locations.
Contains Ordnance Survey data \copyright Crown copyright and database right 2016.} \label{fig:SSGB_map_subset}
\end{figure}


Individual SSGB stations with the longest record of complete winters and some other stations are considered (Table \ref{tab:SSGB_long}).
Other stations, in the east and Orkney, were used to investigate the more extreme correlations between the NAO index and UKCP09 snow data (Figure \ref{fig:MOgrid_cor}), accepting that they do not have the longest records.
These results (Figures \ref{fig:SSGB_ind_long} and \ref{fig:SSGB_ind_oth}) corroborate what is shown in the UKCP09 snow results (Figure \ref{fig:MOgrid_cor}); that they all show a negative correlation with the NAO index, with Forrest Lodge, Eskdalemuir and Ardtalnaig showing the strongest correlations.
This is repeated in Figure \ref{fig:SSGB_ind_oth} where eastern sites Crathes and Whitehillocks show a strong relationship with the NAO index.
Also in line with the UKCP09 results, Stenness, chosen because of a poor UKCP09 snow correlation with the NAO index, shows a weak relationship to NAO index (Figure \ref{fig:SSGB_ind_oth}).


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/SSGB_long.pdf}
\caption{Long-record SSGB stations snow cover plotted against the mean DJFM NAO index, shown with a Loess and 95\% confidence bounds.} \label{fig:SSGB_ind_long}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/SSGB_oth.pdf}
\caption{Eastern and Orkney SSGB stations snow cover plotted against the mean DJFM NAO index, shown with a Loess and 95\% confidence bounds.} \label{fig:SSGB_ind_oth}
\end{figure}


SSGB stations Crathes, Eskdalemuir, Forrest Lodge and Whitehillocks have been plotted with linear regression lines in Figure \ref{fig:SSGB_lm}.
Line slopes vary from -7 to -14 days for higher elevations and from -6 to -16 days for lower elevations.
Some results in Figures \ref{fig:SSGB_curves} to \ref{fig:SSGB_ind_oth} show the NAO index has a larger impact at lower elevations, but Pearson correlation values are variable.
This could be a function of stations not observing the same time periods and hence some sampling produces better correlations than others.
None of the SSGB stations recorded snow cover during the record low NAO index winter of 2009 to 2010.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/SSGB_lm.pdf}
\caption{Comparison between days snow cover at select SSGB stations in years that reported all months between November and April and the mean DJFM NAO index. Shown with a linear model with 95\% confidence bounds and a Loess (dark grey) for comparison.} \label{fig:SSGB_lm}
\end{figure}


%---------------------------------------------------------------------------------
\subsection{MODIS}

Figure \ref{fig:MODIS_cor} shows the correlation between NAO index and snow cover (a) and cloud cover (b) from both MODIS satellites; these results were aggregated to a 5 km resolution, to better show correlations.
Figure \ref{fig:MODIS_cor}a shows a generally weak correlation between MODIS snow cover and the NAO index.
The strongest correlations are in north west Scotland, with the weakest in central eastern Scotland.
Orkney shows a strong correlation, in contrast to the UKCP09 and SSGB results.
A small proportion of the plot, east of Edinburgh, has a very weak but positive correlation, in disagreement with Figure \ref{fig:Bonacina_box} to \ref{fig:SSGB_lm}.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter4/figures/MODIS_NAO.pdf}
\caption{Correlation between number of days of a) snow and b) cloud cover recorded by MODIS each winter (Nov to Apr) and the mean DJFM NAO index.} \label{fig:MODIS_cor}
\end{figure}


Differences from UKCP09 and SSGB results are most likely because of the frequency of cloud, as it is difficult for visible remote sensing to see through cloud.
The problem is illustrated in Figure \ref{fig:MODIS_cor}b, which shows cloud cover as interpreted by MODIS correlated with the NAO index.
The area of positive correlation exceeds the area of negative correlation.
An east-west split in correlation is clearly shown, with the east coast negatively correlated to the NAO index and the west coast positively correlated to the NAO index.
This will have an impact on seeing spatial snow cover trends; if the east of Scotland gets more days of snow cover when there is a negative NAO index, a corresponding increase in cloud cover will obscure snow observations.


%---------------------------------------------------------------------------------
\subsection{Data comparison}

A comparison of correlations from different datasets can be seen in Table \ref{tab:NAO_correlations}.
These results are summarised by Pearson correlations between datasets.
Correlations between SSGB and UKCP09 are 0.87 and between SSGB and MODIS are -0.07; demonstrating that the SSGB and UKCP09 results corroborate each other, but that MODIS results do not correlate with SSGB results.


\begin{table}[htbp]
\caption{Pearson correlations of snow cover and NAO indices at SSGB stations with geographically corresponding values extracted from MODIS and UKCP09 rasters.} \label{tab:NAO_correlations}
\centering
\begin{tabular}{llllr}
  \hline
Station & Elevation & SSGB & UKCP09 & MODIS \\ 
  \hline
Ardtalnaig & high & -0.20 & -0.41 & -0.40 \\ 
  Ardtalnaig & low & -0.27 & -0.41 & -0.40 \\ 
  Couligarton & high & -0.18 & -0.30 & -0.34 \\ 
  Couligarton & low & -0.10 & -0.30 & -0.34 \\ 
  Crathes & low & -0.43 & -0.52 & -0.33 \\ 
  Crathes & high & -0.37 & -0.52 & -0.33 \\ 
  Derry Lodge & low & -0.23 & -0.22 & -0.53 \\ 
  Derry Lodge & high & -0.13 & -0.22 & -0.53 \\ 
  Drummuir & high & -0.52 & -0.46 & -0.53 \\ 
  Drummuir & low & -0.52 & -0.46 & -0.53 \\ 
  Eskdalemuir & high & -0.38 & -0.49 & -0.30 \\ 
  Eskdalemuir & low & -0.38 & -0.49 & -0.30 \\ 
  Fersit & low & -0.11 & -0.27 & -0.53 \\ 
  Fersit & high & -0.25 & -0.27 & -0.53 \\ 
  Forrest Lodge & low & -0.29 & -0.51 & -0.48 \\ 
  Forrest Lodge & high & -0.32 & -0.51 & -0.48 \\ 
  Stenness & high & 0.02 & -0.05 & -0.51 \\ 
  Stenness & low & 0.02 & -0.05 & -0.51 \\ 
  Whitehillocks & high & -0.41 & -0.55 & -0.54 \\ 
  Whitehillocks & low & -0.50 & -0.55 & -0.54 \\ 
   \hline
\end{tabular}
\end{table}


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Discussion}

There is a strong correlation between UKCP09 and SSGB results, with highlighted areas like south west Scotland and east Scotland showing strong negative correlations between snow cover and the NAO index and Orkney with no correlation.
This indicates that UKCP09 methodology is appropriate for analysing the spatial relationship between snow cover and NAO phase at a national scale.
SSGB data have shown stronger correlation between the NAO index and snow cover at lower elevations.
I believe this is because lower elevations have more transient snow as they are generally warmer than higher elevations, so snow will be less likely to fall and lying snow will more readily melt.
This makes snow in these areas susceptible to even small changes in temperature.
Perhaps most importantly, the persistence of snow at lower elevations is less, because increases in temperature from westerly air flows have a greater impact on areas that are closer to melt.
This low elevation correlation is supported, by proxy, by the Bonacina snowiness index correlation with the NAO index (Figure \ref{fig:Bonacina_box}) as the majority of Great Britain is low lying, so the Bonacina snowiness index is more likely to reflect the more common (lower) elevation zone than more remote mountain areas.
The correlations presented in this chapter of NAO index and snow cover are weaker for higher elevations, which are often cold enough for deeper snow to accumulate and take longer to melt for a wider range of typical winter temperatures.
The most recent example of this was winter 2013/14, which was comparatively mild and very wet, but vast quantities of snow fell at higher elevations in Scotland \citep{Kendon2015}.
\citet{Kendon2015} discuss a lapse rate of approximately 6 \textdegree C/km between Aviemore and Cairngorm summit, which was linked to the persistent Atlantic weather type and absence of temperature inversions. This lapse rate is higher than the long-term (1983 to 2008) average of 5.2 \textdegree C/km for Aviemore and Cairngorm chair lift calculated by \citet{Burt2010}, helping to explain the depth and duration of snow cover accumulated that winter.

Inland areas generally have a poorer correlation with the NAO index.
As much of this area is high in elevation this can partly be attributed to it being cold enough for snow to accumulate and persist, irrespective of the NAO index.
These areas further from the coast may also be dominated more by local weather systems and micro-climates, enabling snow to persist for longer.

Those stations that showed a more easily defined relationship with a Loess have had linear models fitted (Figure \ref{fig:SSGB_lm}), with Pearson correlation values from -0.29 to -0.5.
This range of results could be explained by micro-climates having a bigger impact on snow cover than long-term weather patterns.
This would be especially true on the east side of the Cairngorms, where snow driven by wind (predominantly westerly) often accumulates on eastern slopes and can take a long time to melt.
These spatial local discrepancies can also be temporal; given that the SSGB sites did not all observe the same winters, some may have been more closely correlated with the NAO index than others.
The obvious solution is to consider the results from Figure \ref{fig:SSGB_curves}, which average over a greater number of SSGB stations, helping to reduce uncertainty.

