# Thesis plan

## Chapter 1 (introduction)

Set the scene
Why snow is important
Motivation
Conditions required for snow and likely synoptic set up for snow fall/accumulation.
Aims and objectives - crucially; how I achieve them

## Chapter 2 (data description)

What do we need data for?

Perhaps change headings so they cover snow, precip, temp and other? Would make it easier to have a few sentences about alternative datasets and why I choose the ones I did.

SSGB transcription
SSGB stations, longest records, trends, snow days/elevation curves

## Chapter 3 (data validation)

SSGB three close stations
SSGB snow elevation curves
SSGB through time
SSGB vs Bonacina
SSGB vs MOgrid
SSGB vs MODIS
Water balance for Mar Lodge
OS Terrain50 vs Panorama

## Chapter 4 (NAO)

Why consider NAO? (what else is it used for?)
Why is it useful?
Where else considers NAO or other circulation patterns?
Intro to data not previously described
NAO to temp and precipitation
Nation wide
Drill down to specific locations
Importance of results

## Chapter 5 (Modelling)

Modelling specific data validation? e.g. CEH GEAR missing precip
Data preparation
Model schematic
Equifinality of simple model, likely lumped elevations
Model melts snow too quickly to match 50% cover of SSGB obs
Incorporation of density and depth
Stochastic model run - likely parameter sets?
Running on a grid and comparing to SSGB

## Chapter 6 (Stats and model results)

Areas of highest potential snowmelt, do these just correlate with elevation?
Which winters had higher melts, did these correspond to colder or warmer years? Possibly need to aggregate melt nationwide
Can the model output say anything about the 2010 vs 2013 winters? Maximum stored snow? How do they compare to 1992 winter?

## Chapter 7 (Discussion & conclusion)

What other avenues?
