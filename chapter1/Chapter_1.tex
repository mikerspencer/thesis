%---------------------------------------------------------------------------------
%	CHAPTER One: Introduction (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Introduction]{Introduction}
\chaptermark{Introduction}
\label{ch:introduction} % label for referring to chapter in other parts of the thesis


\newpage


% ----------------------------------------------------------------
% ----------------------------------------------------------------


\section{Background}

\begin{spacing}{1}
\begin{quote}
Snow originates in clouds at temperatures below the freezing point.
As moist air rises, expands and cools, water vapour condenses on minute nuclei to form cloud droplets [of] the order of 10 microns in radius.
\ldots
Once a droplet has frozen it grows quickly at the expense of the remaining water droplets because of the difference in saturation vapour pressure between ice and water.
The form of the initial ice crystal, columnar, plate-like, dendritic, etc. depends on the temperature of formation.
\ldots
After deposition snow may dissipate rapidly by melting or sublimation or it may persist for long periods.
If it persists it will undergo metamorphism, changing its grain texture, size and shape, primarily as a result of the effects of temperature and overburden pressure as it becomes buried by subsequent snowfalls. \citep{Armstrong2008}
\end{quote}
\end{spacing}

The above quote summarises the snow accumulation and melt process.
Snow is part of the water cycle in parts of the world where precipitation falls at temperatures around or below 0 \textdegree C.
Snow is important for a number of reasons, including: water storage, melt and land cover.
The following examples are from areas with widespread snowy conditions, consequently showing some of the greatest impacts of snow.

This thesis investigates the influences and magnitudes of snowmelt and snow cover in Scotland.
Snowmelt and snow cover are important for flooding and water supply, climate indicators and habitat change.
The following three sections provide a literature review to demonstrate this importance, the first two sections in a global context and the final one for Scotland.


% ----------------------------------------------------------------

\subsection{Importance of snowmelt}

Snowmelt has caused Europe-wide flooding.
\citet{Brazdil2010} investigated the Little Ice Age winter of 1783 to 1784.
They found that, following large snow accumulation several phases of flooding passed across Europe between December and April caused by rain-on-snow events and rising air temperatures.
The initial flooding phase began in more temperate areas, like England, France and the Netherlands, spreading through central Europe as spring arrived.
Investigations into more recent (1950 to 2011) European rain-on-snow flooding and associated trends have found that, as spring snowfall has decreased and winter rainfall has increased, the early winter flood risk in medium-elevation mountain ranges has increased \citep{Freudiger2014}.

On the west coast of the United States, much of the annual precipitation falls as snow between November and April.
This is then released gradually as snowmelt, providing water through the year \citep{Hughes1992}.
Compared to the 2000 to 2012 average snow water equivalent (SWE), the years 2012 to 2014 were 60, 33, and 18\% respectively \citep{Molotch2015}.
This reduction of water storage as snow is causing widespread drought in California.


% ----------------------------------------------------------------

\subsection{Importance of snow cover}

Snow cover can have an impact on climate; \citet{Matsumrua2014} have shown that earlier spring snowmelt in Eurasia is leading to an increase in land surface temperature by reducing albedo.
This then causes intensified anticyclonic circulation, which has contributed to a reduction in Arctic sea ice.

The Intergovernmental Panel on Climate Change (IPCC) reports \citep[Section 4.5]{IPCC_ch4} on seasonal snow using these indicators: snow cover extent; the seasonal sum of daily snowfall; snow depth; snow cover duration (the number of days with snow exceeding a threshold depth); and snow water equivalent.
They summarise that northern hemisphere snow cover is decreasing, most notably in spring, due to increasing temperatures.

Snow cover in temperate climates also has an important role to play.
\citet{Bicknell2006} describe the Australian skiing industry as a ``canary in a coal mine'' and look at the industry response to climate change.
This includes: artificial snow making, development of higher terrain and non-snow related winter activities.
Scotland has a temperate climate \citep{McClatchey2014}, which is subject to temperatures above and below freezing and precipitation depths up to a few metres per year.


% ----------------------------------------------------------------

\subsection{Snow research in Scotland}

Snow is important in Scotland for water resources, e.g.: the largest instrument-measured flow in Scotland's largest catchment, the River Tay, was partly caused by snowmelt \citep{Black1993}.
\citet{Dunn2001} show that snow can contribute to river baseflow until July, as melted snow generally takes a slower sub-surface pathway to a water course.
Also, \citet{Gibbins2001} and \citet{Helliwell1998} discuss the importance of snowmelt for freshwater invertebrate habitat in the Cairngorms.
Winters with increased snow cover tend to mean a more acidic environment, which is then flushed through the river system during subsequent snowmelt periods, making for harsher conditions for invertebrates \citep{Helliwell1998}.

\citet{McVean1958} finds that snow cover affects vegetation in several ways: it re-distributes precipitation; shortens the growing season with prolonged snow cover; causes spring irrigation with melt water; offers protection from frost; and deposits wind-blown mineral and plant debris from snow beds.
Therefore, knowledge of snow extent and duration can help understand habitat change \citep{Trivedi2007}.
In a changing climate, mammals with a seasonally varying coat colour will find it increasingly difficult to match their camouflage to land cover \citep{Mills2013}, because the period when the animal is white to match the snow may no longer match the times when there is snow cover present.
Knowing the duration of current snow conditions is crucial for understanding how future changes may impact these species.

Snow avalanches and debris flows can shape terrain.
In the Lairig Ghru (Cairngorms) snow avalanches are locally significant geomorphic events \citep{Luckman1992}, although, their impact is often confined to reworking existing debris flow material.
In the west of Scotland on Ben Nevis, there exists a 75 m boulder rampart formed at the end of an extensive avalanche chute \citep{Ballantyne1989}.
This rampart dams a lochan (small water body), which is thought to have been excavated by repeated avalanche impact, the spoil of which forms the rampart, along with other avalanche debris.

\citet{AlHassan1999} used meteorological and traffic data for the Lothians region (around Edinburgh) between 1987 and 1991 to compare traffic volume and weather conditions.
They found lying snow caused a 10\% weekday and a 15\% weekend reduction in traffic activity.
Snow also has an impact on recreation.
\citet{Harrison2001} reported shortening of the ski season in winters leading to 2001.
An update to this publication is due as some winters since then have been very snowy \citep[e.g.][]{Prior2011}.

Finding or collecting meaningful snow data in Scotland is non-trivial.
\citet{Green1973} gives a list of disparate observation types of Scottish snow data: 1) number of days when snow fell; 2) number of days of snow lying (by definition: over half of the ground snow-covered at 0900 GMT in the immediate neighbourhood of the station); 3) average depth of snow in the neighbourhood of the station; 4) altitude of the general snow-line.
They describe reconciling these observations as a challenge, given that not all are available everywhere; this issue is confounded as none of these variables describe the amount of water available in the snow pack.
Challenges in Scottish snow observation continue today as it is difficult to remotely sense predominantly wet snow in mountainous terrain through frequent cloud cover.
For a detailed discussion on remotely sensing snow in Scotland, see Section \ref {section:other_data} and \ref{section:SSGBvsMODIS}.

Given the difficulty in collecting traditional snow data, one approach has been to look at the number of snow patches which survive through the summer to the following winter.
\citet{Watson1994} examined summer snow patches and climate in northeast Scotland for the years 1974 to 1989 and found snow patch persistence, i.e. the number remaining through the summer, strongly correlated (0.66 to 0.91) with winter and spring temperatures, and spring snow-drift.
\citet{Green2009} found similar controls of snow patches in the Snowy Mountains, Australia: winter snow accumulation and summer temperatures.
Theses Australian snowpacks were found to have declined significantly over a 54 year period causing previously perennial snow patches to melt in 2006.

\citet{Jackson1977a} used snow depth observations collected by the UK Met Office to derive estimates of two and five year return period snow depth, SWE and snowmelt.
They used 50 Met Office stations, which each had greater than 15 years of observations.
Five year return period values reported by \citet{Jackson1977a} included a snow depth of approximately 35 cm and SWE of 60 mm for high ground areas of Scotland.
\citet{Jackson1977a} also summarise a number of studies which have looked at snow densities.
Snow densities given were between: 50 and 200 kg/m\textsuperscript{3} for freshly fallen snow, 200 and 300 kg/m\textsuperscript{3} for snow a few weeks old, and exceed 400 kg/m\textsuperscript{3} during periods of rapid thaw.

Unsatisfied with the low estimates of SWE reported in \citet{Jackson1977a}, \citet{Ferguson1985} undertook a spring snow survey in the River Feshie catchment of the Cairngorms between February and May 1984.
He found extremely high snow densities: from 270 kg/m\textsuperscript{3} for what appeared to be fresh powder to 630 kg/m\textsuperscript{3} for late lying snow in late April and mid May.
He suggests a combination of wind compaction and freeze-thaw cycles led to these high figures.
Other work \citep{Green1973} on Scottish snow has found a change in its persistence, with an increase in snow cover duration in the months of November and April.
These are based on observations from the decades either side of 1960.
\citet{Green1973} finds the increase in snow cover mainly attributable to decreasing temperatures and increasing precipitation.

\citet{Arnell1996} made an early study on the effects of climate change on UK river flow using projections from the UK Climate Change Impacts Review Group.
They found that by 2050, snowfall and snowmelt would be almost eliminated from UK hydrology, but in arriving at this conclusion they used only four river gauging stations in Scotland.
Three of these were located in the Southern Uplands (south of Edinburgh) and the remaining one drains the east side of the Cairngorms (River Don).
In more recent work \citep[e.g.][]{Hannaford2005,Hannaford2006} the effect of climate change on snowmelt and resulting river flows has not been addressed, but they did find significant increases in winter flows in Scotland.
\citet{Hannaford2008} discuss the possibility of whether this flow increase may relate to a decrease in precipitation falling as snow, or a decrease in the duration of snow cover, but do not investigate specifically what impact this may have had on river flows.

One thing these papers \citep{Hannaford2005,Hannaford2006,Hannaford2008} all have in common is they find a stronger correlation between river flows and the North Atlantic Oscillation (NAO) index, than with year.
The NAO summarises an atmospheric pressure difference, which is strongly related to the UK experiencing weather systems from the east (negative NAO phase) or west (positive NAO phase).
For in-depth information on the NAO see Chapter \ref{ch:NAO}.
A long record (1861 to 1991) of precipitation is presented by \citet{Macdonald2006}, who correlate it to, amongst other things, the westerly wind force (WWF).
They find WWF strongly correlated (r=0.896, p<0.01) with NAO, for the period 1951 to 2002; and that significant correlations between precipitation and WWF are strongly positive in the west of Scotland (0.45 to 0.77) and insignificantly negative in the east (-0.04 to -0.14).
However, these precipitation with NAO correlations nearly exclusively deal with rainfall and do not investigate the relationship with snow.

The above review shows that better constraining snow cover and snowmelt and its influences is fundamentally important.
This is especially the case for Scotland where existing knowledge is underdeveloped, yet snow has a major role to play.


% ----------------------------------------------------------------
% ----------------------------------------------------------------


\section{Thesis aim and objectives}

Scottish snow is important; yet how it is changing and the degree of relationship to atmospheric circulations are not understood.
This thesis aims to:
\textbf{demonstrate the importance of snow in a temperate climate - case study Scotland.}
This will be achieved by the following objectives:

\begin{itemize}
\item Show that a volunteer-collected, snowline-observation dataset can be used to quantify snow duration and melt
\item Map and quantify the relationship between snow and the NAO index
\item Quantify extreme value statistics of Scottish snow, i.e. snow cover and snowmelt.
\end{itemize}

These problems are addressed by using a range of data sources, which are outlined in Chapter \ref{ch:data_desc}.
These data sources are validated and their suitability assessed (Chapter \ref{ch:data_validation}).
In Chapter \ref{ch:NAO}, correlations between Scottish snow and the NAO index, including the spatial variability and an estimate of impact are presented.
The datasets shown in Chapter \ref{ch:data_desc} do not include observations of SWE or snowmelt; to derive these a snow accumulation and melt model was constructed, which is detailed in Chapter \ref{ch:model}.
This model also produces results where observations are sparse or do not exist.
The results from this modelling exercise are shown in Chapter \ref{ch:results}, which focuses on temporal trends and extremes of snow cover and melt, and correlation with NAO.
Finally, in the conclusions, problems and opportunities for further work appear in Chapter \ref{ch:conclusion}.
