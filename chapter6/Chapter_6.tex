%---------------------------------------------------------------------------------
%	CHAPTER Six: Results (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Modelling results]{Snow modelling results}
\chaptermark{Modelling results}
\label{ch:results} % label for referring to chapter in other parts of the thesis

\newpage


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Introduction} \label{section:results_intro}


This chapter presents the results from the snow accumulation and melt modelling in Chapter \ref{ch:model}, which culminated in a 5 km gridded model running across Scotland for winters beginning 1960 to 2010.
Outputs from this modelling are daily snowmelt and snow cover.
As discussed in Chapter \ref{ch:introduction} the presence of snow cover has an impact on ecology \citep[e.g.][]{Trivedi2007} and snowmelt plays an important role in water quality \citep[e.g.][]{Helliwell1998} and extreme hydrological events \citep[e.g.][]{Black1993}.

The aim of this chapter is to use the modelling results of Chapter \ref{ch:model} to understand the statistical probability of extreme snow cover and melt across Scotland.
Pearson correlations are used to show relationships and trends, with the latter being shown as correlations with year.
This chapter is split into sections covering snowmelt and snow cover.
Each section is then divided by general observations and correlations of the results, and an extreme value analysis.


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Snowmelt}


% ----------------------------------------------------------------

\subsection{Melt per annum}


In order to gauge the importance of snow to Scottish hydrology, the proportion of precipitation as snowmelt was calculated.
This describes more than how much solid or liquid phase precipitation fell, as snow can fall and not accumulate, hydrologically behaving as rain.
As the model was run at a daily time step, accumulated snow must lie for a minimum of one day, which would delay runoff generation, but this may not reflect reality.

Figure \ref{fig:melt_precip_med} shows the median, lower and upper quartiles of winter (Oct to May) modelled snowmelt as a proportion of precipitation for winters beginning 1960 to 2010.
Note that there is uncertainty in higher elevation estimates due to the underestimation of precipitation by CEH GEAR (see Chapter \ref{ch:model}).
This has an uncertain impact on these figures as higher summer precipitation would lower these percentages, but may be balanced by winter precipitation also being underestimated.
The underestimation of winter precipitation is likely greater than summer, given the difficulty of snow precipitation measurement \citep{Goodison1997}.
For the snowiest areas, melt values vary little between Figures \ref{fig:melt_precip_med}a to \ref{fig:melt_precip_med}c, with maximum values of 42, 38 and 47\% (median, lower and upper quartiles).
There is more variation for median values of snowmelt as a proportion of precipitation, with figures of 3.6, 1.7 and 6\% for Figures \ref{fig:melt_precip_med}a to \ref{fig:melt_precip_med}c.
These results seem spatially coherent too, with high elevation areas in the east of Scotland (e.g. the Cairngorms) getting the highest proportion of snowmelt.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/melt_precip_med.pdf}
\caption{1960-2010 winter (Oct to May) snowmelt as a percentage of annual precipitation: a) median, b) lower quartile, c) upper quartile.} \label{fig:melt_precip_med}
\end{figure}


Figure \ref{fig:melt_precip_cor} shows winter (Oct to May) snowmelt as a proportion of annual precipitation, correlated against a) year and b) mean DJFM NAO index.
Both figures use the same colour scale and it is apparent that correlations are generally stronger with the NAO index (-0.69 to 0.47) than with year.
Strong negative correlations (-0.54) exist against year (Figure \ref{fig:melt_precip_cor}a), but these are less prevalent than the negative correlations of Figure \ref{fig:melt_precip_cor}b.
Decreases in precipitation as snow were reported anecdotally in \citet{Black1995}, over an undocumented time period.
Figure \ref{fig:melt_precip_cor}a has a single cell of positive correlation (0.34) around Ben Nevis, Scotland's highest peak (1345 m ASL).
This suggests that, despite warming temperatures, Ben Nevis is high enough that it remains cold enough for increased precipitation \citep{Macdonald2006,Zhang2007} to fall as snow.
The strong negative correlations of \ref{fig:melt_precip_cor}b support the findings of Chapter \ref{ch:NAO} that a negative NAO phase winter brings more snow to Scotland.
The strong positive correlations in Figure \ref{fig:melt_precip_cor}b, show that despite a positive phase NAO meaning warmer winter weather is more likely, some western mountain areas are cold enough for the increased precipitation of a positive NAO phase to fall as snow.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/melt_precip_cor.pdf}
\caption{1960-2010 winter (Oct to May) snowmelt as a percentage of annual precipitation correlated against (a) year and (b) DJFM NAO index. Only correlations significant at p < 0.05 are shown.} \label{fig:melt_precip_cor}
\end{figure}


% ----------------------------------------------------------------

\subsection{Extreme value statistics}\label{section:EVA}


Annual (Oct to May) maxima (AMAX) snowmelt values were extracted for each grid cell, resulting in a 51 year series.
A generalised extreme value distribution (GEV) was fitted using l-moments for each cell \citep{Gilleland2011}.
Snowmelt values were derived for 50, 20 and 1\% AEP (annual exceedance probability) from the fitted GEV for each grid cell; these are shown in Figure \ref{fig:melt_AEP}.
Median and maximum snowmelt values from each grid are: a) 10 and 65, b) 15 and 83, and c) 25 and 112 mm/day.
As discussed in Chapter \ref{ch:model}, these values are subject to uncertainty as the CEH GEAR data underestimates precipitation at higher elevations; it is likely they underestimate the maximum melt.
A modelling reason for this is the high value of d.50 used, which enables the model to match observations without needing to melt as much snow.
Due to the heterogeneous nature of snow cover, particularly in a mountain environment, the large grid cells used will also underestimate snowmelt as there is no sub-cell parameterisation; i.e. as ground is uncovered by melting snow there is a positive feedback loop due to darker ground absorbing more heat energy and melting snow faster \citep{Essery1999b}.
Higher melt values would be in line with \citet{Archer1981}, who estimated snowmelt by runoff up to 144 mm/day at Moorhouse in NE England.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/melt_AEP.pdf}
\caption{Snowmelt for (a) 50\%, (b) 20\% and (c) 1\% AEP.} \label{fig:melt_AEP}
\end{figure}


Higher rates of snowmelt are not confined to higher elevations.
Figure \ref{fig:AMAX_elev} shows 50, 20 and 1\% AEP plotted against elevation.
As can be seen, elevations below 400 m ASL are capable of nearly the highest snowmelt rates.
These cells are located in western and northern Scotland and as they do not have particularly large lower return period melt rates (Figure \ref{fig:AMAX_elev}a and b), the fitted GEV curve must be steeper than at higher elevations.
Presumably, then, these areas are more sensitive to winter temperature than predominantly colder areas like eastern Scotland.
This means that in some winters these cells are above freezing and snow does not accumulate, but during other winters they are below freezing - but continue to be wet - and large amounts of snow can accumulate.
The behaviour of these areas is likely transferred to other locations and elevations in warmer and colder winters.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/AMAX_elev.pdf}
\caption{Modelled melt with annual exceedance probabilities of (a) 50\%, (b) 20\% and (c) 1\% plotted against elevation, where each point represents a 5 km grid cell.} \label{fig:AMAX_elev}
\end{figure}


20\% AEP snowmelt was compared to the \citet{Hough1997a} regression estimate, which used two weather factors: maximum mean daily January air temperature and mean daily January wind speed.
A 20\% AEP melt rate was used for comparison as these were the  only regression equations provided by \citet{Hough1997a}.
1980 to 2010 long term average gridded temperature and wind speed data were downloaded from the Met Office UKCP09 site\footnote{\url{http://www.metoffice.gov.uk/climatechange/science/monitoring/ukcp09/download/index.html}} and the regression was scripted in the R language.
The UKCP09 grid dataset used for the \citet{Hough1997a} regression and model input have the same origin and spacing, meaning a direct comparison of cell values is straightforward.
Figure \ref{fig:Hough_vs_mod} shows my 20\% AEP snowmelt estimate compared to \citet{Hough1997a}, plotted with a one to one line.
As \citet{Hough1997a} used a large number of low elevation stations, these elevations should have a greater degree of confidence than the lower elevation results presented here.
As shown in Figure \ref{fig:AMAX_elev}, lower elevations generally have lower snowmelt, so the bottom left of Figure \ref{fig:Hough_vs_mod} has the highest confidence.
The inflection in Figure \ref{fig:Hough_vs_mod} could relate to the elevation in the model that tends to always be cold enough for snow.
However, at this point, \citet{Hough1997a} snowmelt is approximately double that from my model; as discussed previously, this could be due to an underestimate of higher elevation precipitation.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/Hough_vs_mod.pdf}
\caption{Comparison of 20\% AEP melt between \citet{Hough1997a} and model output, where each point represents a 5 km grid cell in the same location. \citet{Hough1997a} estimates are derived from a two weather factor (temperature and wind) regression.} \label{fig:Hough_vs_mod}
\end{figure}


A brief peaks over threshold (POT) assessment was made (Figure \ref{fig:POT_time}), where the number of cells that exceeded a melt of 42 mm/day was summed for each year.
A value of 42 mm/day was chosen as this is used as a snowmelt design maximum in UK flood estimation \citep{FSR1975,FEH1999}.
There are no apparent trends, other than a possible reduction in POT cell counts from the 1970s.
The indication here is that 42 mm/day is probably an acceptable design maximum for low elevation situations, but not higher elevations.
However, it must be noted again that these values of snowmelt are likely underestimates.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/POT_time.pdf}
\caption{The number of cells each year exceeding a melt rate of 42 mm/day.} \label{fig:POT_time}
\end{figure}

% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Snow cover}

\subsection{Snow cover per annum}

The number of days with snow cover was extracted for each year and for each grid cell.
These were correlated against year and NAO index (Figure \ref{fig:snowdays_cor}).
The correlation range for year (Figure \ref{fig:snowdays_cor}a) was -0.46 to 0.53 and for NAO (Figure \ref{fig:snowdays_cor}b) -0.65 to 0.22, but none of the positive correlations with NAO were significant at p < 0.05.
Supporting the results of Figures \ref{fig:melt_precip_med}, \ref{fig:melt_precip_cor} and \ref{fig:AMAX_elev}, the highest correlation area for snow cover against year is Ben Nevis.
Some lowland, coastal areas also show a positive correlation, but these are not significant and are weak (less than 0.25).
Strong correlations between snow cover and the NAO phase are prevalent and negative.
These negative correlations support the results of Chapter \ref{ch:NAO}.
As the modelling results do not have spatial or temporal gaps, the results shown in Figure \ref{fig:snowdays_cor}b are possibly more important in defining the relationship between NAO and snow cover than the results of Chapter 4.
However, the modelling results are subject to the undefined uncertainties of the modelling approach.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/snowdays_cor.pdf}
\caption{The number of days of snow cover each year correlated against a) year and b) mean DJFM NAO index. Only correlations significant at p < 0.05 are shown.} \label{fig:snowdays_cor}
\end{figure}


% ----------------------------------------------------------------

\subsection{Extreme value statistics}


As outlined in Section \ref{section:EVA}, extreme value analysis was repeated for the number of days with snow cover each winter (Oct to May).
Figure \ref{fig:snowdays_AEP} shows the: a) 50\% and b) 1\% AEP for snow cover each winter.
The median and maximum number of days snow cover for 50\% AEP are 10 and 207 and for 1\% AEP are 74 and 240.
As for snowmelt (Section \ref{section:EVA}), the GEV fit for snow cover duration in the snowiest areas is flat compared to less snowy areas.
This flat GEV fit results in little variation in snow cover duration between frequently occurring and rare return periods in the snowiest areas, particularly the Cairngorms (Figure \ref{fig:snowdays_AEP}).
However, the inverse is true for less snowy areas, where the GEV fit is steeper and rarer snow cover events are much greater than frequently occurring ones.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter6/figures/snowdays_AEP.pdf}
\caption{a) 50\% and b) 1\% AEP for the number of days of snow cover.} \label{fig:snowdays_AEP}
\end{figure}


% ----------------------------------------------------------------
% ----------------------------------------------------------------

\section{Discussion}

The results of this chapter are novel and warrant publication.
The general decrease of snow cover in eastern Scotland and a localised increase in snow cover in western Scotland supports an east-west precipitation trend split, where the west is getting wetter and the east drier \citep{Macdonald2006}.
The high proportion of precipitation as snowmelt (up to a median per annum of 42\%) emphasises the importance of snow in Scottish hydrology and that more research is justified.

Estimated values of extreme snowmelt are important and those presented in Section \ref{section:EVA} would benefit from refining when better estimates of high elevation precipitation are available.
Others have previously considered the veracity of a design snowmelt rate of 42 mm/day for UK engineering hydrology, a digest of this work follows:
\citet{Archer1981} summarises the work of the Met Office in deriving extreme snowmelt estimates for use in dam design and other structures affected by floods:
A snowmelt rate of 42 mm/day/m\textsuperscript{2} was assumed as a realistic design maximum for annual exceedance probabilities (AEP) over 2\% in the Flood Studies Report \citep{FSR1975}, which was then adopted as a design maximum in the Flood Estimation Handbook \citep{FEH1999}.
This figure was reached by three methods: 1) by determining the melt rate from the duration of thaw of a measured initial SWE; 2) by converting snow depth to SWE, assuming a snow density and thereby determining daily rates of decline in water equivalent; 3) by determining snowmelt rate as a function of maximum temperature.
\citet{Hough1997a} test the assumption of the guidance that a 42 mm/day maximum melt rate should be applied across the UK.
They used hourly reporting Met Office sites to provide melt totals for durations between three and 168 hours (including 24 hours); these were supplemented with high elevation climate stations to make further 24 hour estimates.
An extreme value analysis was completed using these annual melt maxima and the AEP of 42 mm/day estimated.
AEPs varied between 10\% (Pennines and Scotland) and less than 0.1\% (low elevation England).
Finally they developed four linear models of snowmelt, dependant on altitude, northing, mean January temperature and windspeed.
Their model using two weather parameters, temperature and windspeed, achieved the lowest RMSE (6.56 mm).
From these studies and the work presented in this chapter, it becomes apparent that a single potential snowmelt rate for all of Great Britain is not appropriate.
In some areas the snowmelt risk is underestimated by a large margin, but in most areas structures designed to a 42 mm/day standard are over engineered.

Extending the analysis presented in this chapter to the winter of 2013 to 2014 would be particularly interesting.
During the 2013 to 2014 winter there was widespread flooding in southern England, while there were deep accumulations of snow in upland areas of Scotland.
These snow accumulations resulted in many snow patches remaining on Scottish hills into the next winter \citep{Cameron2015}.
However, an extension of this work will only be possible when the Met Office and CEH release further years of data to use as input for a snowmelt model.
A logical next step is to re-run these analyses, beginning with the model runs from Chapter \ref{ch:model}, with a range of possible parameter values to give uncertainty bounds for snow cover and snowmelt estimates.
