%---------------------------------------------------------------------------------
%	CHAPTER Two: Data (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Data descriptions]{Data summary and description}
\chaptermark{Data}
\label{ch:data_desc} % label for referring to chapter in other parts of the thesis


\vfill
\small{Author Contributions: some of the work presented in this chapter has previously been published \citep{Spencer2014}.
Richard Essery contributed major edits to the publication and Met Office authors contributed typographical corrections.}
\newpage

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Chapter contents} \label{section:data_desc_contents}

This chapter covers the datasets used in this research, with a focus on the Snow Survey of Great Britain (SSGB).
The SSGB was the initial spark for this research; as an unused data source in a field with little data availability there are many possibilities for its use.
A history of the SSGB, the area observed in Scotland, the transcription process and digital data availability are presented.
Other datasets described comprise of snow cover, precipitation, temperature, mapping and a climate proxy. A proportion of this chapter is derived from \citet{Spencer2014}, which can be found in Appendix \ref{ch:Appendix_pub}.
Discussion on the appropriate application of the differing datasets is in Chapter \ref{ch:data_validation}, along with data validation.

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Data overview} \label{section:data_overview}

Data used in this research fall into two broad categories: point observations and grids.
An example of the former are data collated by the Met Office from their network of automated gauges and observers, including variables such as when snow lies on the ground each day, daily precipitation, minimum and maximum temperatures.
An example of gridded data is where the Met Office point observations have been interpolated to infill the gaps between stations.
Other examples of gridded data are maps and remotely sensed aerial images, e.g. those collected by satellite.
Ephemeral snow in Scotland makes metrics like average snowline and beginning and end of continuous snow cover for a given winter meaningless, because each winter can see many snow accumulation and melt cycles.
A solution to this is to use a count of the days of snow cover during a given time period; this is the approach generally taken in this research.
Unless described otherwise, days with snow lying have been derived as snow cover from October to May over each winter.
As this work is concerned with larger snowmelt events and longer duration snow cover, then focusing on the snowiest part of the year is most appropriate.
There is interest in snow cover during the summer months, particularly from an ecological perspective, but cover is confined to small patches \citep{Watson2011}.

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Snow Survey of Great Britain} \label{section:SSGB_desc}

The SSGB is a voluntary observer collected dataset which recorded snow cover each winter (usually October to May) between 1937 and 2007.
Volunteers were based at estates, the water authorities, Nature Conservancy (now Scottish Natural Heritage), energy companies, Forestry Commission, and others; they often also collected other weather observation data for use by the Met Office.
The SSGB was used to produce the annual publication 'Report on the Snow Survey of Great Britain' between 1947 and 1992.
The title of this varied through time but the content was consistent; an example is \citet{Hawke1949}.
The annual SSGB reports from autumn 1953 until spring 1992 are available from the Met Office\footnote{\url{http://www.metoffice.gov.uk/learning/library/archive-hidden-treasures/snow-survey}}.
Until 2014, most SSGB data only existed in paper form and little use had been made of them.
\citet{Jackson1978} used the SSGB to discuss the frequency and extent of snow cover in Great Britain.
\citet{Jackson1977} also used SSGB data to help complete a snow index of years from 1875/76 to 1974/75.
\citet{Trivedi2007} transcribed data for the Ardtalnaig station on Loch Tay for use in vegetation analysis, undertaking data quality assurance by checking other meteorological stations within the station vicinity.
\citet{Trivedi2007} found that further use of the SSGB would be warranted as it gave a deeper insight into climate change.

%---------------------------------------------------------------------------------
\subsection{History of the SSGB} \label{section:SSGB_history}

The Snow Survey of Great Britain began in 1937 \citep{Jackson1978} and was directed by Mr. Gordon Manley \citep{IJG32}.
After a hiatus during World War Two, the snow survey was officially resumed in autumn 1946 by the British Glaciological Society.
The principal aim was to 'secure representative data relating to the occurrence of snow-cover at different altitudes in the various upland districts of Great Britain over the period October to June' \citep{IJG32}.
The reorganisation of the survey was undertaken by Mr. E.L. Hawke, Honorary Secretary of the Royal Meteorological Society and a member of the British Glaciological Society and a request for help made in 1947 \citep{IJG32}; however paper copy data exist in the Edinburgh Met Office archive from the Autumn of 1945.

In 1953 the collation of data by the British Glaciological Society ceased and was thereafter undertaken by the British Climatology Branch of the Meteorological Office \citep{MetMag1954}.
\citet{Hawke1954} report in their final snow survey summary that the number of sites had increased from 120 to nearly 400, including land stations, lighthouses and light-vessels.

Between 1946/47 and 1991/92 an annual report was produced summarising the data returns for the season.
Until 1954 this report was issued by the British Glaciological Society.
From 1954 onwards the Met Office produced the annual SSGB report.
The survey was administered by the Met Office from the Scottish Weather Observations Centre in Edinburgh, where data were also collated.
In 1992, due to the dwindling interest and lack of funding, the annual publication was withdrawn.

Despite the withdrawal of the annual summary publication, data continued to be collected until 2007.
In 1994 there was a review of the 77 participating stations and those deemed not to view high ground or those that duplicated other stations were withdrawn from the survey.
32 stations in Great Britain remained after the review.
The observer instructions were also updated following the 1994 review; the most important change was that volunteers were no longer required to note when an observation was obscured by cloud or fog or the observer was absent, although some continued to do so.
The last SSGB records stored in the Edinburgh Met Office archives were observed in May 2007.
It is not documented why the SSGB was terminated, but I speculate that funding cuts and a lack of use are the main reasons.

Scottish data between Autumn 1945 and Summer 2007 are stored in the Met Office archives in Edinburgh.
These records pre-date the official commencement of the survey in 1946 as noted by \citep{IJG32}.
A likely reason for this is that stations continued reporting snow cover during the Second World War, after the initial snow survey beginning in 1937.
Some earlier records have been located in the Gordon Manley papers archive\footnote{\url{http://endure.dur.ac.uk:8080/fedora/get/UkDhU:EADCatalogue.0534}}, but these have not been viewed or transcribed.
The Met Office archive in Exeter holds records for English and Welsh stations between 1946 and 1992. I believe the SSGB ceased due to a combination of budget cuts and lack of use of the collected dataset.


%---------------------------------------------------------------------------------
\subsection{Coverage of the SSGB} \label{section:SSGB_coverage}

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/SSGB_map.pdf}
\caption{Location of Scottish SSGB stations colour graded by record length in years.} \label{fig:SSGB_map}
\end{figure}

The SSGB was collected across Great Britain, but this study has only transcribed Scottish records, as few English and Welsh records are kept in the Edinburgh archives and snow falls more often in Scotland.
Records for over 140 sites in Scotland were found within the Met Office archive; the most southerly is Kirkbean near Dumfries and the most northerly is Collafirth Hill on the Shetland Isles.
The elevation range from which observations were made is from sea level to 724 m ASL (above sea level), at Lowther Hill near Wanlockhead.
After examining the locations and observing dates for the SSGB stations, some were found to be the same station, but with a name change - presumably through different volunteers having different opinions.
The following changes were made (showing years reported under that name): Shin (1964) into Cassley Power Station, Ardclach (1946) into Glenferness, Tarfside (1958) into Glen Esk.
These three stations were straightforward to combine as they were geographically very close and the longer running stations had missing data when the shorter running ones were recording.
There is a possibility that Dykecrofts and Newcastleton are the same station, however this is less clear as there is a distinct name change, so perhaps the station moved within the village.
Figure \ref{fig:SSGB_map} shows the spatial distribution of the recording stations, with each station colour graded to indicate its record length.
Table \ref{tab:SSGB_longest} details the 10 stations with the longest records; note these are different to those which have the most data available, as some records are incomplete.

\begin{table}
\caption{10 longest recording SSGB stations.} \label{tab:SSGB_longest}
\centering
\begin{tabular}{lp{3.5cm}p{2cm}lr}
\hline
Name & Visible hills & Observing years & Start & End \\
\hline
Couligarten & Ben Lomond & 47 & 1954 & 2006 \\
Loch Venachar & Ben Ledi & 47 & 1954 & 2004 \\
Eskdalemuir & Ettrick Pen & 46 & 1954 & 2005 \\
Forrest Lodge & Creag Meagaidh & 46 & 1954 & 2005 \\
Sourhope & Cheviot & 44 & 1954 & 2003 \\
Ardtalnaig & Ben Lawers & 41 & 1954 & 2004 \\
Fersit & Corserine & 41 & 1954 & 2002 \\
Hopes Reservoir & Pentlands & 41 & 1957 & 2002 \\
Stronachlachar & Stob a' Choin & 38 & 1954 & 1997 \\
Glengyle & Ben Venue & 36 & 1954 & 1993 \\
\hline
\end{tabular}
\end{table}


The SSGB observers looked out on the hills that surrounded their location and noted at what level snow was lying.
Elevations were grouped into 150 m bands from 0 to 1200 m ASL or 500 feet increments earlier in the record, with most stations supplying metric returns by the early 1980s.
The observers were asked (taken from January 1992 instructions) to record at 09:00 GMT ``or thereabouts'' when snow or sleet was falling at station level and if snow was lying at station level, with depth.
Lying snow was to be recorded at visible elevations when it covered greater than half the ground at a given elevation.
Finally they were asked to record when fog or cloud obscured observation.
These instructions are shown in Figure \ref{fig:SSGB_instructions}.
The results of this process can be seen in Figure \ref{fig:SSGB_return}, an example return card from Dalwhinnie; note the visible hills listed.
Figure \ref{fig:SSGB_instructions} also shows comments from the observer that for nine days they did not make observations from the station.
This comment highlights a challenge of this dataset, that these observations are not standardised.

\begin{figure}[htbp]
\centering
% draw box around pdf
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
\fbox{\includegraphics[width=1\textwidth]{./chapter2/figures/SSGB_instructions.pdf}}
\caption{Example SSGB instructions. Contains Met Office data \copyright Crown copyright and database right 2016.} \label{fig:SSGB_instructions}
\end{figure}

\begin{figure}[htbp]
\centering
% draw box around pdf
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
\fbox{\includegraphics[width=1\textwidth]{./chapter2/figures/SSGB_return.pdf}}
\caption{Example SSGB return from Dalwhinnie in October 1980. Contains Met Office data \copyright Crown copyright and database right 2016.} \label{fig:SSGB_return}
\end{figure}

I have assessed the area visible from each SSGB site using line of sight analysis in the GIS software GRASS \citep{GRASS}.
Using the Panorama digital terrain model (Section \ref{section:OS_desc}), an area was calculated which shows the land visible from each SSGB station (e.g. Figure \ref{fig:SSGB_3st_loc}) based on grid reference and a viewing elevation of 10 m.
An elevation much higher than a person's viewpoint was chosen in the belief it would offset uncertainty in station location and the expectation that observers may not view hillslopes from the exact station location, but may move themselves to get a better view.
The visible areas were combined for the 140 sites and split into SSGB elevation bands.
Each SSGB visible area band was then divided by the area of Scotland in that elevation band, giving percentages of each elevation band visible.
The total fraction of Scotland visible from SSGB sites is 10.1\%.
These are compared to the number of Met Office stations reporting snow lying in each elevation band (Table \ref{tab:SSGBvsMO}).
The SSGB covers a greater proportion of higher than lower elevations and the Met Office stations are the inverse of this, in-line with the 1946 aims of the survey \citep{IJG32}.

\begin{table}[htbp]
\caption{Percentage of each elevation band in Scotland, percentage of each elevation band visible from SSGB stations, compared to percentage of Met Office stations recording snow (total 281) sited in each elevation band.} \label{tab:SSGBvsMO}
\centering
\begin{tabular}{llp{2cm}p{2.3cm}}
\hline
Elevation (m ASL) & Scotland (\%) & SSGB visible (\%) & Met Office Stations (\%) \\
\hline
0 to 150 & 40 & 11 & 75 \\
150 to 300 & 29 & 9.1 & 21 \\
300 to 450 & 17 & 9.1 & 3.2 \\
450 to 600 & 8.1 & 9.6 & 0.36 \\
600 to 750 & 4.1 & 9.8 & 0.36 \\
750 to 900 & 1.6 & 12 & 0 \\
900 to 1050 & 0.36 & 16 & 0 \\
1050 to 1200 & 0.074 & 23 & 0 \\
1200 and above & 0.008 & 32 & 0 \\
\hline
\end{tabular}
\end{table}

From studying the returns and the annual reports it appears that some hard copy data are missing.
While disappointing, it is unsurprising as the paper records have changed hands and locations through the years.
Figure \ref{fig:SSGB_winter} shows the number of stations in Scotland for which paper copies exist, by winter.
Data are missing from 1994 and only three station records were found which covered the whole winter.
This coincided with the station review and perhaps there was confusion over which stations were still to submit reports.
Annual SSGB summary reports before 1955 indicate nearly 400 stations across Great Britain, but fewer than 30 Scottish stations were found in the Edinburgh archives.
According to \citeauthor{Jackson1978} \citeyearpar{Jackson1978}, there are data from 1937 onwards; some of these are in the Manley archives.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/SSGB_winter.pdf}
\caption{Number of Scottish SSGB stations, with data available by winter, found within the Scottish Met Office archives.} \label{fig:SSGB_winter}
\end{figure}

%---------------------------------------------------------------------------------
\subsection{Transcription of the SSGB} \label{section:SSGB_digitisation}

For each station encountered, metadata from the SSGB return sheets were noted.
This information was: site name, elevation (m ASL), easting (m), northing (m), hills visible and comments.
These data are useful for identifying sites and establishing what was visible from each location.
The comments section was used to record notes on data quality.
For example, Brig-O-Turk recorded the lowest lying isolated snow patch, not the level of snow cover greater than 50\%.
Brig-O-Turk also noted where continuous snow lay in the comments; this value was used in the transcription.
Where noted, missing values occurring when an observation was obscured by poor visibility or the observer was absent were transcribed.
However, these cannot always be distinguished from when there was no snow.
The paper copy returns were transcribed into a spreadsheet with each column representing a station and each row representing a day.
Data transcription took approximately three months and approximately 16750 return sheets (one sheet for each station, each month) were input.
Quality assurance was undertaken to check for typographical errors, but no further data checks were undertaken.
Following transcription, data were uploaded to the Met Office database MIDAS (Met Office Integrated Data Archive System), and are now managed by the Met Office and are available through the British Atmospheric Data Centre\footnote{\url{http://badc.nerc.ac.uk}}.
For this research data were written to a SQLite\footnote{\url{https://sqlite.org/}} database.
SQLite was chosen for its cross platform compatibility and the availability of libraries for most analytical programming languages \citep[e.g.][]{RSQLite}.

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Other data sources} \label{section:other_data}


%---------------------------------------------------------------------------------
\subsection{Met Office stations} \label{section:MO_desc}

Met Office data are meteorological observations sampled at point locations and are often only available for lower elevations.
Datasets collected include: temperature, precipitation and snow.
Snow data collection generally requires a human observer to be present, although the Met Office now operate approximately five automatic snow gauges in Scotland.
Snow data were collected at manual Met Office weather sites by observers who noted if snow was lying at the station, and if so with what depth.
Snow data collected could include snow presence (binary), snow depth (cm) and fresh snow depth.
Precipitation data used for this research are 24 hour accumulations, i.e. the precipitation that has fallen between 09:00 GMT on subsequent days.
It is notoriously difficult to measure the amount of different kinds of precipitation, particularly snow \citep{Doesken2009}.
Problems with undercatch of snowfall become apparent during modelling.
Minimum and maximum daily temperature are observed at 09:00 GMT each day.
In a hope of overcoming precipitation undercatch, only staffed Met Office stations were used as these would collect lying snow data and hopefully have better representation of precipitation during snowfall, as staff were available to melt snow and measure the resulting depth of water.
Met Office stations with long records that were spread across Scotland were sought.
Data were subset to only include winters (October to May, inclusive) which had complete observations of temperature, precipitation and snow.
Met Office station data were downloaded from the British Atmospheric Data Centre\footnote{\url{http://badc.nerc.ac.uk}} for the stations shown in Table \ref{tab:MOstdata}.
All Met Office stations recording lying snow are shown (Figure \ref{fig:MOst_map}), with those used in this research highlighted.


\begin{table}[htbp]
\caption{Met Office station data used, showing location details and the number of winters (Oct to May) with complete daily observations of temperature, precipitation and snow depth.} \label{tab:MOstdata}
\centering
\begin{tabular}{lp{2cm}p{2cm}p{2cm}p{3cm}}
\hline
Name & Easting (m) & Northing (m) & Elevation (m ASL) & Complete winters (with snow depth > 0) \\
\hline
Braemar & 315200 & 719400 & 339 & 4 \\
Dalwhinnie & 263941 & 785427 & 351 & 2 \\
Eskdalemuir & 323498 & 602638 & 236 & 1 \\
Inverailort & 176418 & 781616 & 2 & 3 \\
Knockanrock & 218694 & 908816 & 244 & 3 \\
\hline
\end{tabular}
\end{table}


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/MOst_map.pdf}
\caption{Location of Scottish Met Office stations recording lying snow, colour graded by elevation in metres.} \label{fig:MOst_map}
\end{figure}

%---------------------------------------------------------------------------------
\subsection{UKCP09 interpolated grids} \label{section:UKCP09_desc}

The UK climate projections 2009 (UKCP09) grid dataset comprises a 5 km resolution raster image for each month; where each grid value represents a climate parameter for that cell, over a given time period.
These grids were interpolated from Met Office station data by \citet{Perry2005}, using a combination of regression and inverse distance weighting.
Factors used in the regression included: easting and northing, terrain elevation, mean altitude over a 5 km radius of the station, percentage of open water within a 5 km radius of the station, and the percentage of urban land use within a 5 km radius of the station.
Two UKCP09 datasets have been used: snow cover data, where each cell value is the number of days in a month with snow cover; and average temperature, where each cell value is the mean of minimum and maximum daily temperature.
Full winter (October to May) snow cover data are available from winters beginning 1971 until 2005.
The UKCP09 snow lying data have been shown \citep{Spencer2014} to compare poorly to the SSGB at higher elevations, most likely due to interpolation from low lying stations which do not adequately represent mountain snow.
The UKCP09 snow lying grids are useful for broad nationwide assessments, helping to identify regions for further study \citep{Spencer2014}.
UKCP09 grid data were downloaded from the Met Office\footnote{\url{http://www.metoffice.gov.uk/climatechange/science/monitoring/ukcp09/download/index.html}}.

%---------------------------------------------------------------------------------
\subsection{Moderate-resolution Imaging Spectroradiometer} \label{section:MODIS_desc}

Data from satellite instruments are used to derive global snow cover products, available from 1966 onwards \citep{Matson1991}. There are two main methods for remote sensing of snow; microwave and visible.

Visible satellite remote sensing methods are not ideal for measuring snow cover in Scotland because snow cannot be viewed through the frequent cloud cover.
Windows of opportunity for sampling may occur less than once a week \citep{Slater1999}. 
Working in North America, \citet{Tang2010} found that MODIS (Moderate-resolution Imaging Spectroradiometer, \citealt{Hall2002})  had the greatest uncertainty measuring snow covered area during the autumn and spring months, when snow was accumulating or ablating. 
\citet{Dong2010} investigated the relationship between air temperature and MODIS snow covered area error; as expected from the findings of \citet{Tang2010}, error increased with temperature.
This error was quantified to be 80\% for temperatures above 15 \textdegree C, reducing to 10\% for temperatures below 0 \textdegree C or -5 \textdegree C depending on location.
This is of particular note for remote sensing of snow in Scotland where temperatures do not often stay far below freezing.
Snow in Scotland is often wet, which also provides a challenge to microwave satellite observation.
\citet{Rees2001} found that for some types of vegetation cover, notably that without trees, they were able to use remote sensing to detect wet snow by considering a reduction in backscatter attributable to the snow.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/MODIS_eg.pdf}
\caption{Example images taken on 2010-02-20 from a) the MODIS instrument on the Terra satellite and b) combined with data from the Aqua satellite to reduce cloud pixels.} \label{fig:MODIS_eg}
\end{figure}

Using microwaves to detect snow cover is very challenging in mountainous terrain \citep{Snehmani2015} or when snow is wet \citep{Rees2001}. \citet{Snehmani2015} review methods that improve microwave assessment of snow cover, but these are data and computing intensive and trialling them in Scotland where it is very cloudy, wet and mountainous is beyond the scope of this study.
There are some snow cover products that amalgamate different data sources, including \citet{Robinson_CDR} and \citet{Foster2011}.
The former of these is a 190.5 km grid resolution and the latter a 25 km grid resolution.
Both of these are coarse grid sizes which would miss spatial detail.
\citet{Foster2011} found that the Earth Observation System MODIS, outperformed microwave snow detection in cloud free areas.
MODIS is freely available on a 500 m grid at a twice daily resolution, one image from the Aqua satellite and one from the Terra satellite.
There are some reanalysis products, e.g. \citet{Notarnicola2013}, which recalculated snow cover from MODIS observations at a 250 m grid, however these are only available for the Alps.
MODIS data were chosen for use in this study because: of the time overlap with SSGB data, it is a dataset still being collected and the fine resolution grid it is available on.
MODIS data were downloaded from the National Snow and Ice Data Centre \citep{Hall2006}.
The MODIS dataset chosen is the tile set which records as binary whether snow covered each cell, rather than the fractional or albedo datasets.
Coverage of Scotland is split across two tiles; these were downloaded for both the Aqua (2002-07-04 onwards) and Terra (2000-02-24 onwards) satellites.
Each pair of tiles were merged together and warped to the British National Grid projection using GDAL \citep{GDAL}.
These raster images were then managed in the GIS GRASS \citep{GRASS}, where combination images of both satellites were created to reduce the incidence of cloud pixels, e.g. Figure \ref{fig:MODIS_eg}.
A cloud pixel reduction of approximately 15 \% was achieved when one satellite recorded cloud by taking a cell value from the other satellite.
This method was only possible from 2002-07-04 onwards, when the Aqua satellite became operational.
Prior to this the Terra satellite alone was used, creating a dataset containing full winters from 2000/01 until 2013/14.
Cloud pixel reductions are shown in Figure \ref{fig:MODIS_cloud}, where Figure \ref{fig:MODIS_cloud}a shows the monthly number of cloud pixels from the Aqua and Terra satellites are broadly equal and Figure \ref{fig:MODIS_cloud}b compares Terra and Aqua cloud pixel counts against the combined dataset, showing a marked (approx 15\%) difference.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/MODIS_cloud.pdf}
\caption{Cloud pixel reduction (km\textsuperscript{2}) achieved by combining MODIS snow cover data from the Terra and Aqua satellites. a) Shows the number of cloud pixels per month from the Aqua and Terra satellites are roughly equal. b) Shows that by combining data from Aqua and Terra the number of cloud pixels per month can be reduced (approx 15\%). The improvement is the difference from the 1:1 line.} \label{fig:MODIS_cloud}
\end{figure}

%---------------------------------------------------------------------------------
\subsection{Bonacina snowiness index} \label{section:Bonacina_desc}

The Bonacina snowiness index was originally compiled by Leo Bonacina \citep{Bonacina1966,Jackson1977} and is now maintained as a website\footnote{\url{http://www.neforum2.co.uk/ferryhillweather/bonacina.html}}.
It categorises the snowiness of each winter into four subjective categories: Little, Average, Snowy and Very snowy.
These categories are based on how much snow fell and how much of Britain it covered, using anecdotal data from weather journals, Met Office stations and websites.
In this respect it is different to the other snow cover datasets used in this work, as other datasets present snow cover duration.
I have used it because it covers a much longer time period than the other snow cover datasets, beginning in 1875 and still being maintained.
The category of each winter is shown in Figure \ref{fig:Bonacina_time}.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/Bonacina_time.pdf}
\caption{Bonacina snowiness categories through time.} \label{fig:Bonacina_time}
\end{figure}

%---------------------------------------------------------------------------------
\subsection{North Atlantic Oscillation index} \label{section:NAO_desc}

North Atlantic Oscillation (NAO) index data were downloaded from the Climate Research Unit\footnote{\url{http://www.cru.uea.ac.uk/cru/data/nao/}} (1821-2000) and Tim Osborn's NAO website\footnote{\url{http://www.cru.uea.ac.uk/~timo/datapages/naoi.htm}} (1999 onwards).
NAO data have been averaged (mean) over DJFM, as described by \citet{Osborn1999}, to better represent the prevailing winter NAO index.
\citet{Osborn1999} uses a DJFM winter NAO index as there is greater interdecadal coherence than with other periods, which is more appropriate for the large temporal scales this work considers.
A summary of these data is shown in Figure \ref{fig:NAO_timehist}; the predominant winter NAO index is positive (128 positive winters, 65 negative winters), aligning with our understanding that the UK is more likely to experience weather systems approaching from the west.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/NAO_timehist.pdf}
\caption{Mean DJFM NAO a) through time b) histogram.} \label{fig:NAO_timehist}
\end{figure}


%---------------------------------------------------------------------------------
\subsection{River level and flow} \label{section:SEPA_desc}

River level and flow data are collected in Scotland by the Scottish Environment Protection Agency and collated at a UK scale by the Centre for Ecology and Hydrology (CEH).
Datasets are available to download from the CEH hosted National River Flow Archive (NRFA).
These include: catchment boundaries, annual maximum flow and mean daily flow.
Daily flow data for the Mar Lodge catchment, high on the river Dee (Aberdeenshire), have been downloaded \citep{NRFA} for the period 1982-09-10 to 2014-09-30.
This catchment's key characteristics are shown in Table \ref{tab:MarLodge} and the catchment location is shown in Figure \ref{fig:MarLodge_map}.

\begin{table}[htbp]
\caption{River Dee at Mar Lodge catchment characteristics.} \label{tab:MarLodge}
\centering
\begin{tabular}{llr}
\hline
Parameter & Value \\
\hline
Station ID & 12007 \\
Grid reference (m) & 309788, 789522 \\
Catchment area (km\textsuperscript{2}) & 289 \\
Period of record & 1982 - ongoing \\
Station elevation (m) & 332 \\
Maximum catchment elevation (m) & 1309 \\
Annual average rainfall (1961-1990) (mm) & 1335 \\
\hline
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter2/figures/MarLodge_map.pdf}
\caption{Location of River Dee catchment at Mar Lodge.} \label{fig:MarLodge_map}
\end{figure}


%---------------------------------------------------------------------------------
\subsection{Gridded Estimates of Areal Rainfall} \label{section:GEAR_desc}

The CEH GEAR (Centre for Ecology and Hydrology Gridded Estimates of Areal Rainfall) dataset comprises 1 km resolution daily precipitation data in NetCDF format.
The dataset variables are easting (m), northing (m) (or latitude and longitude) and time (days), hence precipitation can be indexed in two dimensional space and time.
The CEH GEAR data were interpolated from Met Office station data \citep{Keller2015} and cover the time period beginning 1890 to present, with a data release lag while each new year of data is interpolated.
No orographic enhancement of precipitation was made during the interpolation, which was accomplished using a natural neighbour method, including a normalisation step against annual average rainfall \citep{Keller2015}.
I have used GEAR data from 1960 onwards, to match the availability of gridded temperature data, and to this end I also resampled GEAR data to a 5 km grid using a bilinear method to match the coarser Met Office grid.
GEAR data are available from \citet{Tanguy2014}.

%---------------------------------------------------------------------------------
\subsection{Ordnance Survey mapping} \label{section:OS_desc}

The Ordnance Survey is the British national mapping agency.
Its datasets are available for free through OpenData\footnote{\url{http://www.ordnancesurvey.co.uk/innovate/innovate-with-open-data.html}} project or for academic use via Edina\footnote{\url{http://edina.ac.uk/}}.
The datasets used in this study are shown within Table \ref{tab:OS_data}.
There are two elevation models detailed: Panorama and Terrain 50.
Terrain 50 superseded Panorama in 2013 and became a maintained data product, as opposed to Panorama which was never maintained after its creation.
I have used both of these elevation models.
An abbreviated copyright will appear on figures using Ordnance Survey data, the full copyright is:

\begin{spacing}{1}
\begin{quote}
Contains Ordnance Survey data. \copyright Crown copyright and database right 2016.
Data provided by Digimap OpenStream, an EDINA, University of Edinburgh Service.
\end{quote}
\end{spacing}


\begin{table}[htbp]
\caption{Ordnance Survey mapping data used.} \label{tab:OS_data}
\centering
\begin{tabular}{lp{8cm}}
\hline
Name & Description \\
\hline
1:250k Scale Raster & Former OS Travel Map in an image format. \\
Land-Form Panorama & Elevation model on 50 m grid. \\
Miniscale & National scale map. \\
Terrain 50 & Current elevation model on 50 m grid. \\
Strategi & Vector data of road and railway networks, cities and rural areas. \\
\hline
\end{tabular}
\end{table}
