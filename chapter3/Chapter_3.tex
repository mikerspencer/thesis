%---------------------------------------------------------------------------------
%	CHAPTER Three: Data validation (C.Stamper template edited by JM)
%---------------------------------------------------------------------------------

\onehalfspace
\chapter[Data validation]{Validation of datasets}
\chaptermark{Data validation}
\label{ch:data_validation} % label for referring to chapter in other parts of the thesis


\vfill
\small{Author Contributions: The analysis comparing SSGB and UKCP09 data presented in this chapter has previously been published \citep{Spencer2014}.
Richard Essery contributed major edits to the publication and the Met Office authors made mainly typographical changes.}
\newpage

%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Chapter contents} \label{section:ch3_intro}

This chapter investigates the datasets used for this research; specifically to check them against each other and consider their appropriate use, but in the case of the SSGB to briefly look at temporal trends.
The chapter begins with the SSGB, including consideration of the inter-site variability of observations using three geographically close stations.
Long term trends in snow cover, as observed by the SSGB, are presented, concluding the SSGB discussion with comments on its viability.
Other available snow datasets are examined; UKCP09, MODIS and Bonacina.
A comparison of the UKCP09 snow grids and SSGB data is part of \citet{Spencer2014}, which is contained in Appendix \ref{ch:Appendix_pub}.
Data used as input for snow modelling are compared to other data sources to check their veracity.
Finally digital terrain models (DTMs) used within this research are compared.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Snow Survey of Great Britain} \label{section:SSGB_trends}

%---------------------------------------------------------------------------------
\subsection{SSGB variability}

Comparing adjacent SSGB stations shows the inter-site variability of SSGB observations.
A number of SSGB stations are geographically close.
Of these, three have approximately 30 years of data and are within 4 km of each other; these three stations are are located to the east of Loch Lomond and are detailed in Table \ref{tab:3st_detail}.


\begin{table}[htbp]
\centering
\caption{Details of three geographically close SSGB stations, visible hills are those noted by SSGB observers.} \label{tab:3st_detail}
\begin{tabular}{lp{1.4cm}p{1.6cm}p{1.6cm}rrp{2.5cm}}
  \hline
Station & Easting (m) & Northing (m) & Elevation (m) & Start & End & Hills visible \\ 
  \hline
Glengyle & 238800 & 713300 & 115 & 1954 & 1993 & Unlisted \\ 
Loch Arklet & 237600 & 709600 & 163 & 1954 & 1993 & Ben Vane \\ 
Stronachlachar & 240100 & 710300 & 126 & 1955 & 1997 & Stob a' Choin \\ 
   \hline
\end{tabular}
\end{table}


To compare the area visible from each station, viewsheds were calculated (Figure \ref{fig:SSGB_3st_loc}) with a GIS line of sight analysis using the Ordnance Survey Panorama data.
A degree of visible area overlap can be seen, but the original SSGB records indicate the Loch Arklet observer was viewing Ben Vane, which can be seen from neither Glengyle or Stronachlachar.
The Stronachlachar observer noted Stob a' Choin as visible, which can be seen from neither Glengyle or Loch Arklet.
However, Glengyle and Stronachlachar both have visibility in the same glen, while not covering the same peaks, in contrast to Loch Arklet which is primarily observing to the west of Loch Lomond.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/SSGB_3st_loc.pdf}
\caption{Location of three long record SSGB stations used to understand correlation in the SSGB dataset.
Showing elevation, roads, water courses and area visible from each station.} \label{fig:SSGB_3st_loc}
\end{figure}


A comparison between these three stations was made by aggregating data monthly, i.e. counting days per month of snowline observations, including cloud and no snow.
There were 264 months where all three stations reported snowline, with observations from 150 to 750 m of elevation.
A table of Pearson's correlations was made (Table \ref{tab:3st_cor}) using all observation counts (150 - 750 m snowline, cloud and no snow) per month; the duration of snow cover ranged from 1 to 31 days.
Glengyle and Stronachlachar, observing the same glen, correlate strongly, but Loch Arklet shows a weaker correlation with both other stations.
From this the inference is that there are contradictory results of inter-station variability.
Some stations may be geographically close, but be observing different hillslope aspects; which results in different snow cover records.


\begin{table}[htbp]
\caption{Pearson correlation matrix between three geographically close and overlapping time period SSGB stations.} \label{tab:3st_cor}
\centering
\begin{tabular}{lllr}
\hline
 & Glengyle & Loch Arklet & Stronachlachar \\ 
\hline
Glengyle & 1.00 & 0.45 & 0.82 \\ 
Loch Arklet & 0.45 & 1.00 & 0.56 \\ 
Stronachlachar & 0.82 & 0.56 & 1.00 \\ 
\hline
\end{tabular}
\end{table}


%---------------------------------------------------------------------------------
\subsection{SSGB trends}

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/SSGB_time.pdf}
\caption{The number of SSGB stations recording all months between November and April each winter.} \label{fig:SSGB_time}
\end{figure}


The SSGB recorded data for a long period of time, but use has largely been confined to annual reports (1953 to 1991 reports are available online\footnote{\url{http://www.metoffice.gov.uk/learning/library/archive-hidden-treasures/snow-survey}}) or isolated station use \citep[e.g.][]{Trivedi2007}.
I think it worthwhile to consider the 60 year record as a whole and whether it shows any trends in snow cover.
To do this only stations which recorded all months between November and April have been used.
While the SSGB generally reported October to May, many stations did not submit October or May reports so considering a slightly shorter period yields more complete station years; 58 years and 124 stations.
The number of stations available peaked around the late 1960s and early 1970s with approximately 70 stations making complete returns each winter (Figure \ref{fig:SSGB_time}).
The record is much less complete at the beginning and end of the time series.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/SSGB_curve.pdf}
\caption{Median snow cover duration curve for all SSGB stations reporting November to April each winter; shaded area shows 25th and 75th percentiles.} \label{fig:SSGB_curve}
\end{figure}


The complete (November to April) SSGB stations have been amalgamated to produce an average winter snow cover duration curve (Figure \ref{fig:SSGB_curve}), i.e. the median number of days with lying snow at a given altitude between November and April.
While not showing trends, this does display a long term Scottish average.
These data have also been split by decade (Figure \ref{fig:SSGB_curve_decade}), showing how snow cover has varied through time.
More recent decades have tended to have fewer days of snow cover each year, with the 2000s showing markedly less snow.
It would be of interest to compare these snow curves to global annual average temperatures and Scottish annual average temperatures, to fit these fluctuations into the bigger climate picture.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/SSGB_curve_decade.pdf}
\caption{Median snow accumulation curves for all SSGB stations reporting November to April each winter, split by decade. Note that the 1940s and 2000s have fewer contributing years and the dip in days of snow lying at 1200 m in the 1940s is caused by sampling fewer stations.} \label{fig:SSGB_curve_decade}
\end{figure}


%---------------------------------------------------------------------------------
\subsection{SSGB viability}

The SSGB is not without its limitations; prominent on this list is observer error.
For example, the observer for Blair Castle Gardens stated on an early submission that they did not have access to a 'local' map giving exact elevations.
While this is unfortunate, there is still great value in Blair Castle station as is provides relative data and the observer would have known the surrounding area well.
In contrast, Crathes station was staffed by Adam Watson between 1979 and 2004, who would have had an excellent understanding of the lie of land and the snow conditions on it, as evidenced by his snow patch work \citep[e.g.][]{Watson2010,Watson2011}.

Known missing data caused by cloud cover, observer absence or a missing return mark time periods of data uncertainty.
What is more challenging are unknown missing data when an observer submitted a monthly return but did not indicate cloud, fog or absence: this would be interpreted as no snow.
When working with a small number of sites or short time period this should be verifiable by correlating general weather observations, particularly cloud cover, visibility and temperature, with gaps in the SSGB record.
For the latter part of the record, observations can be checked against satellite data, although this may not be straightforward: when cloud cover obscured SSGB observations, it could also have obscured visible satellite observations.
This would not be the case with a cloud inversion below the snowline.
Known missing values could be in-filled using machine learning methods like self organising maps \citep{Mwale2012}, although this relies on the SSGB observations and their inherent uncertainty.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{UKCP09 snow compared to SSGB snow cover} \label{section:SSGBvsUKCP09}

\subsection{Method}

A data comparison was made between the UKCP09 snow lying grid and the SSGB dataset as both cover a large range of elevations.
These data were compared for the Dalwhinnie station, chosen as it has a long record (39 years from winters 1967/68 to 2006/07, missing 1994) that overlapped the UKCP09 record, and it has a good range of visible elevations from the Spey valley at ~350 m to Ben Alder at 1148 m, 18.5 km to the south west.
It is likely the Dalwhinnie station collected both the Met Office snow lying data used by UKCP09 and also the SSGB.
Visible elevations were established from the SSGB return and verified by a GIS line of sight analysis using the Ordnance Survey Panorama data, shown in Figure \ref{fig:Dalwhinnie_map}.

UKCP09 data were interpolated from, amongst others, Dalwhinnie station data.
Data collection began on 1973-09-01 and ended on 2007-01-31.
There were whole months missing in October and November 1973, January 1978 and May 1995 until November 1996.
The UKCP09 was interpolated from other reporting stations outside these time periods.
The closest station with snow lying data for the 95/96 winter is Dall Rannoch School, approximately 30 km to the south.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/Dalwhinnie_map.pdf}
\caption{Line of sight analysis for Dalwhinnie SSGB station, showing Ben Alder.} \label{fig:Dalwhinnie_map}
\end{figure}

The monthly UKCP09 data were extracted for the grid cells covering Dalwhinnie and Ben Alder.
These were converted to snow years defined, for the purposes of this comparison, as from the beginning of September until the end of August.
The mean elevations for these two grid cells were calculated from the Ordnance Survey Panorama DTM as 485 m ASL for the Dalwhinnie cell and 821 m ASL for the Ben Alder cell.
The altitude of Dalwhinnie station is 362 m ASL.

SSGB Dalwhinnie data were then lumped into two groups with snowline of 450 m ASL and below and a snowline of 900 m ASL and below, to correspond with snow lying at the elevations of the Dalwhinnie and Ben Alder grid cells.

A summary of the UKCP09 and SSGB datasets for the Dalwhinnie and Ben Alder grid cells is shown in Table \ref{tab:alder_dal}.
In order to fill gaps in the SSGB due to missing returns, the days with snow lying at the Dalwhinnie station were added to the SSGB record for Ben Alder and Dalwinnie.
Days of snow lying per year in the UKCP09 were subtracted from those in the revised SSGB for both Dalwhinnie and Ben Alder.
These differences were plotted as time series with box and whisker plots to show data spread (Figure \ref{fig:Dalwhinnie_difftime}).
For comparison the number of missing observations per year were also plotted.
Missing values comprise two types: those when no monthly return was submitted or has been lost, and when observation was not possible due to observer absence or reduced visibility.
The revised SSGB values were compared to the UKCP09 for Dalwhinnie and Ben Alder (Figure \ref{fig:Dalwhinnie_scatter}) as scatter plots.


\subsection{Results}

Table \ref{tab:alder_dal} compares the number of days with snow lying for the Ben Alder and Dalwhinnie average grid cell elevations using the SSGB and UKCP09.
Of note is the similarity in the number of days with snow lying between Ben Alder and Dalwhinnie according to the UKCP09; this appears unrealistic as snow often falls more frequently and lies for greater periods at higher elevations.
The SSGB values have a greater spread, with the mean value for Ben Alder within 7\% of the UKCP09 maximum.

\begin{table}[htbp]
\caption{Comparison between days of snow lying per winter (1967-2006) at Ben Alder and Dalwhinnie, elevation averaged for 5 km grid cell, using SSGB and UKCP09 data.} \label{tab:alder_dal}
\centering
\begin{tabular}{llllr}
\hline
 & \multicolumn{2}{c}{UKCP09} & \multicolumn{2}{c}{SSGB}\\
\cline{2-5}
 & Dalwhinnie & Ben Alder & Dalwhinnie & Ben Alder\\
\hline
Minimum & 10 & 25 & 23 & 36\\
Maximum & 114 & 120 & 126 & 172\\
Mean & 47 & 60 & 63 & 112\\
Standard deviation & 23 & 21 & 27 & 40\\
\hline
\end{tabular}
\end{table}

Figure \ref{fig:Dalwhinnie_difftime} shows the difference in duration of snow cover between the SSGB and UKCP09 for Ben Alder and Dalwhinnie.
It was expected that the Dalwhinnie difference would be above zero for winters in which data from the station were used in deriving the gridded product because the altitude of Dalwhinnie station is 362 m ASL, while the grid square average is 485 m ASL.
The data distribution for Dalwhinnie is not symmetrical around zero, but has a mean of 14 days and a standard deviation of 21 days.
There is a greater variation in data than expected, as it is reasonable to suppose the SSGB was collected by the same observer who recorded the Met Office station data used to interpolate the UKCP09.
Some of the larger differences coincide with time periods when no Met Office snow lying observations were being made at Dalwhinnie, notably 1971 and 1972. However, other large differences do not match.
The greater difference lies with the Ben Alder grid cell data.
The mean of these differences is 48 days with a standard deviation of 36 days, indicating that the UKCP09 underestimates the duration of snow cover at higher elevations.
An outlier was the 1978/79 winter, during which the SSGB recorded 54 fewer days with snow than the UKCP09 estimated at Ben Alder.
This does not coincide with a year of high missing observations, but the SSGB returns for December, February and March were missing.
The 1979 snow survey report \citep{SSGB1979} describes the season as having frequent snow cover with over twice the 1941-1970 average.
The anomaly is caused by the three months of missing returns during the peak snow season: the Met Office Dalwhinnie station data recorded snow lying for nearly all of February and March.
In total 58 days with snow lying at the Dalwhinnie station were recorded during December, February and March over the 1978/79 winter.
With these added to the SSGB the outlier is reduced.
This process was repeated for other months with missing SSGB returns, shown in Figure \ref{fig:Dalwhinnie_difftime} using a dashed line and asterisk.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/Dalwhinnie_difftime.pdf}
\caption{Difference between SSGB and UKCP09 data at Dalwhinnie and Ben Alder, including median and quartiles.
SSGB data were selected to match the average elevation of each UKCP09 cell.
Where SSGB returns were missing, Met Office station snow data have been added to SSGB records; adjustment is indicated by dashed line from the original SSGB position to the revised value, marked by an asterisk.
Numbers of missing values for the SSGB are also shown.} \label{fig:Dalwhinnie_difftime}
\end{figure}

The two scatter plots comparing sites and datasets in Figure \ref{fig:Dalwhinnie_scatter} show positive correlation.
The sites are most strongly related in UKCP09 data, (Figure \ref{fig:Dalwhinnie_scatter}b) as data at Ben Alder are generally extrapolated from data recorded at Dalwhinnie.
Figure \ref{fig:Dalwhinnie_scatter}a shows a weaker correlation between the datasets at each site than each dataset shows with itself in Figure \ref{fig:Dalwhinnie_scatter}b.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/Dalwhinnie_scatter.pdf}
\caption{a) Comparison between UKCP09 and SSGB numbers of days of snow lying per snow year for each site.
b) Comparison between sites for UKCP09 and SSGB numbers of days of snow lying per snow year.} \label{fig:Dalwhinnie_scatter}
\end{figure}

The UKCP09 snow dataset has value for national assessments.
However, there are two key limitations for use at a local scale: the spatial resolution of the grid is coarse, and the underlying observations used to create the grid have been extrapolated horizontally and vertically.
The 5 km cell covering Dalwhinnie, for example, varies in elevation from 350 m to 858 m with a mean of 485 m.
It is challenging in environmental analysis to work with a single elevation value for a large area, as variation occurs over small vertical and horizontal distances.
With nearly all Met Office snow observations recorded at low elevations and interpolated to mountainous areas, there is uncertainty in a dataset when the grid cell covers an area with a large elevation range.
This is reinforced by the small difference in number of days with snow lying between Ben Alder and Dalwhinnie as given by the UKCP09.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{UKCP09 temperature compared to station data} \label{section:SSGBvsUKCP09_temp}


UKCP09 gridded temperature data are interpolated from Met Office station observations, but these stations are unlikely to represent the average elevation in a grid cell and often do not record the entire period for which UKCP09 data are available.
Table \ref{tab:UKCP09_MIDAS} compares nine, predominantly high altitude, Met Office stations to their corresponding cells in the UKCP09 temperature grid.
Pearson correlations are all strong, a minimum of 0.95, with no obvious relationship to elevation difference between cell mean and station.
This lack of relationship is also true for linear regression model slope and intercept.
However, line slope was expected to negatively correlate to elevation difference; i.e. the lower the cell elevation when compared to station elevation the higher the temperature.
These results could be due to such a small sample size or possibly due to elevation correction during the interpolation process.
Differences between Met Office station observations and UKCP09 cells are within 10\%, and so for use as a temperature input for snow accumulation and melt modelling these data are deemed appropriate.

\begin{sidewaystable}[htbp]
\caption{Correlation and regression line parameters, which compare daily temperature data from UKCP09 and Met Office stations.} \label{tab:UKCP09_MIDAS}
\centering
\begin{tabular}{lp{2.2cm}p{2.2cm}rrrrr}
  \hline
Met Office Station & Met Office Station altitude (m) & Mean UKCP09 5 km cell altitude (m) & Opened & Closed & Correlation & Intercept & Slope \\ 
  \hline
Knockanrock & 244 & 259 & 1963 & 1998 & 0.95 & 0.07 & 0.87 \\ 
  Inverailort & 2 & 236 & 1944 &  & 0.98 & 0.01 & 1.02 \\ 
  Dalwhinnie & 351 & 426 & 1973 &  & 0.98 & -1.70 & 0.98 \\ 
  Cairngorm chairlift & 663 & 559 & 1980 &  & 0.95 & -0.65 & 0.98 \\ 
  Corgarff, castle lodge & 400 & 538 & 1972 & 2002 & 0.98 & -0.90 & 0.98 \\ 
  Braemar & 339 & 479 & 1856 & 2005 & 0.98 & -0.66 & 0.99 \\ 
  Fealar lodge & 560 & 697 & 1963 & 1996 & 0.97 & -4.08 & 1.01 \\ 
  Eskdalemuir & 236 & 326 & 1908 &  & 0.98 & -0.58 & 0.98 \\ 
  Ledmore & 160 & 266 & 2000 &  & 0.96 & -0.80 & 0.92 \\ 
   \hline
\end{tabular}
\end{sidewaystable}


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{MODIS compared to SSGB} \label{section:SSGBvsMODIS}

The SSGB stopped collecting data in 2007 and with a largely automated network of meteorological stations there is no longer any Scotland-wide ground-based collection of snow cover data.
One potential option is to use remotely sensed data, as discussed in Section \ref{section:MODIS_desc}.
Here I test the efficacy of the MODIS snow cover dataset against SSGB.
To do this, SSGB stations which recorded during MODIS operation (2000-02-25 onwards) were selected (Figure \ref{fig:MODIS_SSGB_st}).
As can been seen (Figure \ref{fig:MODIS_SSGB_st}a) the overlapping time period is short, with many of the 21 stations only recording for part of the 1999 to 2007 period.
However, a range of elevations is covered, particularly the 300 to 600 m bands and the stations are spread across Scotland (Figure \ref{fig:MODIS_SSGB_st_loc}).

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/MODIS_SSGB_st.pdf}
\caption{a) SSGB stations with data overlapping the MODIS record. b) Elevations visible from available SSGB stations.} \label{fig:MODIS_SSGB_st}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/MODIS_SSGB_st_loc.pdf}
\caption{Location of the SSGB stations (black circles) which were compared to the MODIS snow cover dataset.} \label{fig:MODIS_SSGB_st_loc}
\end{figure}

The SSGB and MODIS datasets describe different things: the SSGB notes the snowline visible from a single ground based point and the MODIS dataset used classifies grid cells as snow covered or other (e.g. cloud, water, no snow\footnote{For a full list see \url{http://nsidc.org/data/docs/daac/modis_v5/mod10a1_modis_terra_snow_daily_global_500m_grid.gd.html}}).
In order to reconcile these observation differences, SSGB station viewsheds were split into SSGB elevation envelopes (150 m).
Processed MODIS data (as described in Section \ref{section:MODIS_desc}) were sampled for each day and each elevation envelope at each SSGB station, resulting in a percentage area value returned for each variable in an envelope, e.g. 45\% snow cover; 50\% cloud, 5\% no snow.
SSGB observations were reduced to three classes in each elevation band: "Snow", "No snow" and "Cloud".
To see how well existing MODIS observations can predict snow cover a machine learning decision tree was constructed.
The C4.5 method \citep{Quinlan2014} was used to construct the decision tree because of its fast computation time and simple scripting.
The decision tree was implemented using the C50 package \citep{Kuhn2015} in R, which is an updated version of the original \citet{Quinlan2014} C4.5 method.
The decision tree was trained on data between 2000-02-25 and 2004-12-31 and tested on data after this, giving a training set of 84597 observations and a testing set of 14912 observations.
For simplicity, no boosting was applied to the model.
The resulting decision tree from this classification is shown in Figure \ref{fig:DecisionTree}.
Note that while the model was asked to classify observations as cloud, it was unsuccessful because there were few SSGB observations of cloud.

\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/DecisionTree.pdf}
\caption{C5.0 decision tree structure to classify snow presence from MODIS observations, when compared to SSGB observations. The bar charts show the proportion of SSGB observation types for each branch, during the model training. Node 2 and 5 correspond to no snow present and node 4 to snow present.} \label{fig:DecisionTree}
\end{figure}


During the model training run the decision tree was correct 78\% of the time.
The bar charts in Figure \ref{fig:DecisionTree} show that the majority of misclassification came from the presence of snow.
Overall in the training run, SSGB observations of snow were misclassified 42\% and no snow 21\%; 5\% of the SSGB observations were of cloud.
The decision tree performed slightly better during the testing period, matching the SSGB observations 81\% of the time.
Table \ref{tab:MODIS_SSGB_confusion} shows a confusion matrix between the model predictions and SSGB observations for the training dataset.


\begin{table}[htbp]
\caption{Confusion matrix of SSGB observations (rows) and MODIS classified by machine learning (columns) daily observation counts.} \label{tab:MODIS_SSGB_confusion}
\centering
\begin{tabular}{lllr}
  \hline
 & Cloud & No snow & Snow \\ 
  \hline
Cloud &   0 & 4062 &  64 \\ 
No snow &   0 & 64175 & 1376 \\ 
Snow &   0 & 12965 & 1955 \\ 
   \hline
\end{tabular}
\end{table}


A final consideration, for MODIS efficacy, is observations during summer months, when the SSGB did not record.
Table \ref{tab:MODIS_summer} shows broad summer period (JJAS) counts of daily snowline between the dates 2000-02-25 to 2014-12-16; cloud cover is the most prevalent observation.
However, the number of days with a snowline recorded is far in excess of expectations; anecdotally I expected 0 to 5 days with snow at higher elevations during summer months in a 15 year period, with 0 days at lower elevations (600 m and below).
This is supported by annual snow patch counts where approximately six patches remain until the next winter, with patch sizes around 100 m\textsuperscript{2}.
The results from Tables \ref{tab:MODIS_SSGB_confusion} and \ref{tab:MODIS_summer} should be read in conjunction, leading to the conclusion that MODIS snow cover observations are not yet a suitable replacement for ground observations of lying snow in a temperate, cloudy and mountainous environment.
Details of previous research on satellite snow cover efficacy in a areas similar to the Scottish mountains can be found in Chapter \ref{ch:introduction}.

\begin{table}[htbp]
\caption{Total counts of Jun, Jul, Aug and Sep observations from MODIS cells (years 2000 to 2014) overlapping with SSGB stations shown in Figure \ref{fig:MODIS_SSGB_st}.} \label{tab:MODIS_summer}
\centering
\begin{tabular}{lr}
  \hline
Elevation (m) & Days of snow cover \\ 
  \hline
0 & 232 \\ 
150 & 229 \\ 
300 & 233 \\ 
450 & 261 \\ 
600 & 96 \\ 
750 & 66 \\ 
900 & 42 \\ 
1050 & 15 \\ 
1200 & 1 \\ 
Cloud & 27130 \\ 
No snow & 10125 \\
  \hline
\end{tabular}
\end{table}


It would be of value to further explore methods for using MODIS observations in snow covered areas with a cloudy temperate climate like Scotland.
Environments like these are unlikely to commit resources to undertake large scale ground monitoring of snow as cover is often short-lived.
\citet{Notarnicola2013} have classified raw MODIS data to a 250 m grid of snow cover in the European Alps.
They use an algorithm which exploits the 250 m resolution bands of MODIS in the red (B1) and infrared (B2) spectrum, with the Normalized Difference Vegetation Index (NDVI) for snow detection.
Clouds are classified using bands at 500 m and 1 km resolution, which could be adapted for Scotland; although cloud is likely to present a bigger challenge in Scotland than the European Alps.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Bonacina comparison to SSGB} \label{section:Bonacina_SSGB}

The Bonacina snowiness index is a subjective record describing winters over all of Great Britain; here I present how that compares to the SSGB to help to understand the Bonacina dataset's relationship to snow in mountain areas of Scotland.
The snow elevation curves presented in Section \ref{section:SSGB_trends} are here subset by Bonacina category: Very snowy, Snowy, Average and Little (Figure \ref{fig:SSGB_Bonacina}).
As can be seen (Figure \ref{fig:SSGB_Bonacina}), there is a marked difference between the Very snowy and Little categories, but the Snowy and Average groups overlap.
This is likely caused by a bias in the distinction between Average and Snowy winters towards areas of higher population, i.e. central and southern England, whereas further north and at higher elevations these periods would usually already be cold enough for snow to accumulate.
The Bonacina index remains useful for its long record and simplicity, but note the likely limitations for its representation of higher elevation snow cover.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/SSGB_Bonacina.pdf}
\caption{Snow elevation curves derived from SSGB data split into Bonacina snowiness categories.
Shading indicates bounds of 25th and 75th percentiles with solid lines representing the median.} \label{fig:SSGB_Bonacina}
\end{figure}


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Mar Lodge water balance} \label{section:MarLodge}

To help understand the error associated with using CEH GEAR precipitation data in snowmelt modelling, a water balance was undertaken in the River Dee catchment (Aberdeenshire) at Mar Lodge.
Aggregated CEH GEAR data were used, as described in Section \ref{section:GEAR_desc}, as this 5 km grid size was used for snowmelt modelling.
The 5 km grid cells are displayed over the River Dee catchment and river network in Figure \ref{fig:MarLodge_catchment}.
Daily data from each contributing GEAR cell were multiplied by the cell area to get a volume of contributed precipitation.
Daily flow data from the NRFA (Section \ref{section:SEPA_desc}) and CEH GEAR contributing precipitation were summed over water years (October to September); yielding a complete series from water years beginning 1982 until 2011, of input precipitation and output flow.
Summing data to a water year, rather than a shorter time scale, will almost eliminate error from water stored as snow during the winter; although there are a number of small areas of semi-perennial snow in the top of the River Dee catchment \citep{Watson2010} these are insignificant over a large catchment at an annual scale.
Note that there is negligible abstraction, but water is expected to be lost to evapotranspiration and groundwater.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/MarLodge_catchment.pdf}
\caption{The River Dee and tributaries for the catchment at Mar Lodge, overlain with 5 km grid cells from UKCP09.} \label{fig:MarLodge_catchment}
\end{figure}


The annual precipitation and flow data for Mar Lodge were compared (Figure \ref{fig:MarLodge_massbalance}).
Figure \ref{fig:MarLodge_massbalance}a shows GEAR precipitation slightly higher than observed flow: the median precipitation is 106\% of flow.
Evapotranspiration has been estimated using the method shown in Figure \ref{fig:Evap_method}, which is taken from \citet{Kay2008}.
Analysis was completed at a monthly time step and summed to give an annual value for each grid cell.
Input data were taken from UKCP09 monthly average temperature grid cells, using the 22 cells which fell within the Mar Lodge catchment.
The range of annual evapotranspiration for the Mar Lodge catchment is from 300 to 410 mm/yr with a median of 360 mm/yr; the median is equivalent to 27\% of the median annual river flow.
If we assume there is some loss to groundwater, and given the precipitation value would include evapotranspiration this 27\% is broadly comparable with the figure of 18.5\% which \citet{Ferrier1990} calculated for the Allt a'Mharcaidh catchment on the west of the Cairngorms.
When the median annual evapotranspiration is subtracted from precipitation, annual effective precipitation is a median 83\% of annual river flow.
This difference does not include groundwater interaction and is marked, but it is noted that this analysis is for \textit{potential} evapotranspiration only and individual years will likely vary.
Assuming the flow series is more reliable than precipitation, the discrepancy is likely attributable to an underestimation of precipitation at higher elevations.
The reasons for this include: the GEAR dataset derivation method made no orographic rainfall enhancement, interpolation from low elevation stations, and an under-catch of solid precipitation when it occurs.
Flow and precipitation are plotted, coded by colour for number of days per year below 0 \textdegree C (Figure \ref{fig:MarLodge_massbalance}b), to see if larger discrepancies occurred in winters with potentially higher snowfall.
These were assumed to be related to the number of days below 0 \textdegree C; there is little evidence of a trend, meaning that snow is unlikely to be a primary cause of the mismatch in water balance.
Another source of extra water in the catchment is from wind redistribution of snow off the Cairngorm plateau, but at an annual scale in a 289 km \textsuperscript{2} catchment this seems unlikely to be the source of the large difference previously noted.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/MarLodge_massbalance.pdf}
\caption{a) River Dee flow as recorded at the Mar Lodge gauging station and precipitation taken from CEH GEAR data. b) River Dee flow at Mar Lodge against CEH GEAR precipitation, colour coded by the number of days below 0 \textdegree C each year.} \label{fig:MarLodge_massbalance}
\end{figure}

\begin{figure}[htbp]
\centering
% draw box around pdf
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
\fbox{\includegraphics[width=1\textwidth]{./chapter3/figures/Evap_method.png}}
\caption{Taken from \citet{Kay2008}: describing a method to estimate potential evapotranspiration.} \label{fig:Evap_method}
\end{figure}


I have shown that the CEH GEAR dataset likely underestimate the volume of precipitation falling at higher elevations in a catchment on the east of the Cairngorm plateau.
It would be of considerable value to repeat this water balance experiment for other gauging stations across Great Britain to determine if the bias in GEAR is systematic and can be corrected by an elevation and/or spatial multiplier.
Work like \citet{Herrnegger2015}, where they estimated precipitation from runoff, could also be used to derive a correction factor for CEH GEAR precipitation.
These methods should be used in conjunction with observed precipitation lapse rate data from studies like \citet{Ballantyne1983}, who summarise precipitation increases in the Scottish Highlands as 2.81 mm/m on western slopes and 0.88 mm/m on eastern slopes.
\citet{Ballantyne1983} also undertook an experiment on An Teallach in NW Scotland (September 1976 to August 1979), with gauges at three locations between 468 and 671 m ASL on NE facing slopes compared to a low level site at 23 m ASL.
A linear fit between the lowest site and highest gives an increase of 2.67 mm/m in precipitation.
However, when all four elevations are considered, precipitation-increase appears to increase with elevation.
Snow fall was included in these measurements, in all but one period (January to May 1979) when the gauge overflowed and the measurement was lost.


%---------------------------------------------------------------------------------
%---------------------------------------------------------------------------------
\section{Ordnance Survey digital elevation models} \label{section:OSDTM}

As discussed in Section \ref{section:OS_desc}, the Ordnance Survey has two freely available digital terrain models - Panorama and Terrain 50.
Some work presented here had been completed prior to Terrain 50 availability, so this section demonstrates that for a national scale the digital terrain models are similar enough.
Figure \ref{fig:OS_DTMs} presents the difference between the Panorama and Terrain 50 DTMs.
Upland areas show the greatest absolute difference but, given their greater elevation, a much smaller relative difference.
The differences between the two elevation models are generally confined to $ \pm $ 20 m, with exceptions like quarried areas that have large changes, e.g. Loch Leven in Fife.
Some tiles exhibit a greater difference than others, hence some straight lines in Northern Scotland.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/OS_DTMs.pdf}
\caption{Map of differences between Ordnance Survey Terrain 50 and Panorama digital terrain models.} \label{fig:OS_DTMs}
\end{figure}


A cross section line is shown on Figure \ref{fig:OS_DTMs}, across the western Highlands; the elevations along this are presented in Figure \ref{fig:OS_DTM_diff}, with a) showing the full section and b) a 5 km highlight.
As can be seen, there is little difference between the two digital terrain models, although some horizontal shift is evident.
The method Ordnance Survey used to develop these two datasets is a likely cause of differences as the newer Terrain 50 uses a pixel centred value, whereas Panorama took elevation from the edge of each cell\footnote{\url{https://www.ordnancesurvey.co.uk/business-and-government/help-and-support/products/terrain-50.html} see FAQ 9}.
There should also be an improvement in survey accuracy since the Panorama dataset was constructed, hence some differences with the Terrain 50.
Given that Terrain 50 is a maintained data product and Panorama is not, Terrain 50 should be used on new projects, but it does not appear that the difference between these datasets is large enough to warrant re-analysis of previous work.


\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./chapter3/figures/OS_DTM_diff.pdf}
\caption{a) Full and b) highlight of a cross section over Ordnance Survey digital terrain models.} \label{fig:OS_DTM_diff}
\end{figure}

